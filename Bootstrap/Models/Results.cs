﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Bootstrap.Models
{
    public class Results
    {
        static string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }

        public Analyze Compare(Job _job, Resume _Resume)
        {
            Analyze anaysis = new Analyze();

            //get resumes qualification summary
            string Description = _Resume.SingleValues["Description"];

            //get the missing skills from the job - what your resume is missing

            List<string> alljobskills = _job.Skills.Concat(_job.OtherSkills).ToList();
            List<string> actualjobskillsinjob = new List<string>();
            
            foreach (string item in alljobskills)
            {
                Regex myreg = new Regex(@"\b" + item + @"\b", RegexOptions.IgnoreCase);
                var final = myreg.Match(_job.JobText);
                if (final.Success)
                {
                    actualjobskillsinjob.Add(final.Value);
                  
                }


            }

            List<string> missingSkills = actualjobskillsinjob.Except(_Resume.Competencies).ToList();

            List<string> SkillsIntersection = actualjobskillsinjob.Intersect(_Resume.Competencies).ToList();

            //List<string> jobskills = _job.Skills.Concat(_job.OtherSkills).ToList();
            //List<string> notmatched = new List<string>();
           
            List<string> partialmatches = new List<string>();

            foreach (string item in actualjobskillsinjob)
            {
                Regex myreg = new Regex(@"\b" + item + @"\b", RegexOptions.IgnoreCase);
                var final = myreg.Match(_Resume.ResumeText);
                if (final.Success)
                {
                    List<string> vars = new List<string>();
                    vars.Add(final.Value.ToLower());
                    vars.Add(final.Value.ToUpper());
                    vars.Add(UppercaseFirst(final.Value));
                    try
                    {
                        var morethan1 = final.Value.Split(' ');
                        if(morethan1.Length > 1)
                        {
                            string fullword = "";
                            foreach(string mult in morethan1)
                            {
                                string mult2 = UppercaseFirst(mult);
                                fullword += mult2 + " ";
                            }
                            string together1 = fullword.Trim();
                            vars.Add(together1);
                        }
                        if (morethan1.Length > 1)
                        {
                            string fullword = "";
                            for (int i = 0; i < morethan1.Length; i++)
                            {
                                string mult2 = "";
                                if(i==0)
                                {
                                     mult2 = UppercaseFirst(morethan1[i]);
                                }
                                else
                                {
                                    mult2 = morethan1[i].ToLower();
                                }
                                
                                fullword += mult2 + " ";
                            }
                            string together1 = fullword.Trim();
                            vars.Add(together1);
                        }
                        if (morethan1.Length > 1)
                        {
                            string fullword = "";
                            foreach (string mult in morethan1)
                            {
                                string mult2 = mult.ToLower();
                                fullword += mult2 + " ";
                            }
                            string together1 = fullword.Trim();
                            vars.Add(together1);
                        }
                        if (morethan1.Length > 1)
                        {
                            string fullword = "";
                            foreach (string mult in morethan1)
                            {
                                string mult2 = mult.ToUpper();
                                fullword += mult2 + " ";
                            }
                            string together1 = fullword.Trim();
                            vars.Add(together1);
                        }
                    }
                    catch
                    {

                    }
                    partialmatches.Add(final.Value);
                   
                        foreach(string var in vars)
                        {
                            try
                            {
                                missingSkills.Remove(var);
                             }
                             catch(Exception ex)
                            {
                                //cant remove not full match
                            }
                        }
                        
                   
                }

                
            }


            List<string> matched = SkillsIntersection.Concat(partialmatches).ToList();
            List<string> dmatch = (from x in matched
                                   select x).Distinct().ToList();

            List<string> unmatched = _Resume.Competencies.Except(matched).ToList();
            List<string> umatch = (from x in unmatched
                                   select x).Distinct().ToList();

//            List<string> dnmatch1 = (from x in notmatched
//                                   select x).Distinct().ToList();

  //          List<string> dnmatch = dnmatch1.Except(dmatch).ToList();

            

            //Job Required Eduation and YOE
            string requiredDegree = _job.SingleValues["RequiredDegree"];
         
            //int minimumYears = int.Parse(_job.SingleValues["MinimumYears"]);
            int minimumYears = -1;
            if(!int.TryParse(_job.SingleValues["MinimumYears"] as string, out minimumYears))
            if(minimumYears == -1)
            {
                minimumYears = 0;
            }

            //Resume Experience
            //int MOE = int.Parse(_Resume.SingleValues["MonthsOfWorkExperince"]);
            int MOE = -1;
            if (!int.TryParse(_Resume.SingleValues["MonthsOfWorkExperince"] as string, out MOE))
                if (MOE == -1)
                {
                    MOE = 0;
                }
            
            int YOE = MOE / 12;

            //Resume Education
            string highestDegree;
            try
            {
                highestDegree = _Resume.Education.First();
            }
            catch(Exception ex)
            {
                highestDegree = "No Degree";
            }

            
            anaysis.NotMatching = missingSkills.OrderBy(x=>x).ToList();
            anaysis.NotMatchingOther = null;
            anaysis.KeyWordCountJob = dmatch.Count() + missingSkills.Count();
            anaysis.NotMatchCount = missingSkills.Count();
            anaysis.QualificationSummary = Description;
            anaysis.MatchingKeywords = dmatch.OrderBy(x => x).ToList(); ;
            anaysis.Match = dmatch.OrderBy(x => x).ToList(); ;
            anaysis.MatchOther = null;
            anaysis.KeyWordMacthCount = dmatch.Count();
            anaysis.JobYOE = minimumYears;
            anaysis.JobEducation = requiredDegree.ToUpper();
            anaysis.ResumeTax = _Resume.Taxonomies;
            anaysis.ResumeBestTax = _Resume.BestTax;
            anaysis.ResumeEx = _Resume.Experience;
            anaysis.ResumeYOE = YOE;
            anaysis.ResumeEducation = highestDegree;
            anaysis.ResumeCompetencies = umatch;
            //anaysis.ResumeRaw = _Resume.ResumeText;
            //anaysis.JobRaw = _job.JobText;
            
            return anaysis;




        }
    }
}