﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bootstrap.Models
{
    public class Job
    {
       public Dictionary<string, string> SingleValues { get; set; }
       public List<string> CertificationOrLicenes { get; set; }
       public List<string> Skills { get; set; }
       public List<Dictionary<string, string>> Education { get; set; }
       public List<string> OtherSkills { get; set; }
       public string JobText { get; set; }
       public List<string> Taxonomies { get; set; }
       public string BestTax { get; set; }
    }
}