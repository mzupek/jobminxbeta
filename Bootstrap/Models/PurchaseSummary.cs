﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bootstrap.Models
{
    public class PurchaseSummary
    {
        public string plan { get; set; }
        public string orderNumber { get; set; }
        public string partCC { get; set; }
        public string exDate { get; set; }
        public string date { get; set; }
        public int tokens { get; set; }
        public string tokenSummary { get; set; }
        public string total { get; set; }

        public string username { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string address { get; set; }
        public bool offers { get; set; }
        public string ctr { get; set; }
    }
}