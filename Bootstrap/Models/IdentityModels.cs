﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Bootstrap.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        

        public ApplicationUser()
        {
           
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("CloudConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Attempt> Attempts { get; set; }
        public DbSet<UserResume> UserResumes { get; set; }
        public DbSet<Favorite> Favorites { get; set; }
        public DbSet<api> apis { get; set; }
        public DbSet<ActionItem> ActionItems { get; set; }
        public DbSet<Engagement> Engagements { get; set; }

        [Table("UserProfile")]
        public class UserProfile
        {
            [Key]
            [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
            public int UserId { get; set; }
            public string UserName { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string EmailAddress { get; set; }
            public string ProfileImg { get; set; }
            public string Handle { get; set; }
            public int Tokens { get; set; }
            public bool Offers { get; set; }
            public bool Terms { get; set; }
            public bool JobAlert { get; set; }
            public string Plan1 { get; set; }
            public DateTime? CreatedDate { get; set; }
            public string IpAddress { get; set; }
            public DateTime? PlanExperation { get; set; }
            public string PasswordToken { get; set; }

        }

        [Table("Attempt")]
        public class Attempt
        {
            [Key]
            [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
            public int Id { get; set; }
            public string IpAddress { get; set; }

        }

        [Table("UserResume")]
        public class UserResume
        {
            [Key]
            [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
            public int Id { get; set; }
            public string UserId { get; set; }
            public string Name { get; set; }
            public string Resume { get; set; }
            public string ContentType { get; set; }
            public string ZipCode { get; set; }
            public string Category { get; set; }
            public string Experience { get; set; }
            public string Education { get; set; }
            public string Skills { get; set; }
            public string Summary { get; set; }
            public DateTime DateModified { get; set; }
            public DateTime DateLoaded { get; set; }


        }

        [Table("Favorite")]
        public class Favorite
        {
            [Key]
            [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
            public int Id { get; set; }
            public string UserId { get; set; }
            public string ResumeId { get; set; }

        }

        [Table("api")]
        public class api
        {
            [Key]
            [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
            public int Id { get; set; }
            public string Name { get; set; }
            public int Worth { get; set; }
            public DateTime? Date { get; set; }
        }

        [Table("ActionItem")]
        public class ActionItem
        {
            [Key]
            [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
            public int Id { get; set; }
            public int UserId { get; set; }
            public string UserName { get; set; }
            public int PromoId { get; set; }
            public string TransactionId { get; set; }
            public string CardType { get; set; }
            public string Type { get; set; }
            public string Worth { get; set; }
            public string ctr { get; set; }
            public DateTime? PurchaseDate { get; set; }
            public DateTime? Expiration { get; set; }
        }


        [Table("Engagement")]
        public class Engagement
        {
            [Key]
            [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
            public int Id { get; set; }
            public string UserId { get; set; }
            public string Plan { get; set; }
            public DateTime? Expiration { get; set; }
        }
    }

    public class JMContext : DbContext
    {
        public JMContext()
            : base("CloudConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Attempt> Attempts { get; set; }
        public DbSet<UserResume> UserResumes { get; set; }
        public DbSet<Favorite> Favorites { get; set; }
        public DbSet<api> apis { get; set; }
        public DbSet<ActionItem> ActionItems { get; set; }
        public DbSet<Engagement> Engagements { get; set; }
    }

    
    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string ProfileImg { get; set; }
        public string Handle { get; set; }
        public int Tokens { get; set; }
        public bool Offers { get; set; }
        public bool Terms { get; set; }
        public bool JobAlert { get; set; }
        public string Plan1 { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string IpAddress { get; set; }
        public DateTime? PlanExperation { get; set; }
        public string PasswordToken { get; set; }
        
    }

    [Table("Attempt")]
    public class Attempt
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string IpAddress { get; set; }
        
    }

    [Table("UserResume")]
    public class UserResume
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Resume { get; set; }
        public string ContentType { get; set; }
        public string ZipCode { get; set; }
        public string Category { get; set; }
        public string Experience { get; set; }
        public string Education { get; set; }
        public string Skills { get; set; }
        public string Summary { get; set; }
        public DateTime DateModified { get; set; }
        public DateTime DateLoaded { get; set; }

    }

    [Table("Favorite")]
    public class Favorite
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string UserId { get; set; }
        public string ResumeId { get; set; }

    }

    [Table("api")]
    public class api
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public int Worth { get; set; }
        public DateTime? Date { get; set; }
    }

    [Table("ActionItem")]
    public class ActionItem
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int PromoId { get; set; }
        public string TransactionId { get; set; }
        public string CardType { get; set; }
        public string Type { get; set; }
        public string Worth { get; set; }
        public string ctr { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public DateTime? Expiration { get; set; }
    }


    [Table("Engagement")]
    public class Engagement
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Plan { get; set; }
        public DateTime? Expiration { get; set; }
    }
    
}