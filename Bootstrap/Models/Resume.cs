﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bootstrap.Models
{
    public class Resume
    {
        public List<Dictionary<string, string>> Experience { get; set; }
        public List<string> Education { get; set; }
        public List<string> Competencies { get; set; }
        public Dictionary<string, string> SingleValues { get; set; }
        public string ResumeText { get; set; }
        public List<string> Taxonomies { get; set; }
        public string BestTax { get; set; }
    }
}