﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bootstrap.Models
{
    public class Analyze
    {
        
        //Skills comparaison
        public List<string> NotMatchingOther { get; set; }
        public List<string> NotMatching { get; set; }
        public List<string> MatchingKeywords { get;set; }
        public int KeyWordCountJob { get; set; }
        public int NotMatchCount { get; set; }
        public int KeyWordMacthCount { get; set; }
        public List<string> MatchOther { get; set; }
        public List<string> Match { get; set; }
        //Education comparison
        public string ResumeEducation { get; set; }
        public string JobEducation { get; set; }
        public string EducationAI { get; set; }
        //YOE Comparison
        public int ResumeYOE { get; set; }
        public int JobYOE { get; set; }
        public string YOEAI { get; set; }
        
        public string QualificationSummary { get; set; }

        public string ResumeRaw { get; set; }
        public string JobRaw { get; set; }
        public List<string> ResumeTax { get; set; }
        public string ResumeBestTax { get; set; }
        public List<Dictionary<string,string>> ResumeEx { get; set; }
        public List<string> JobTax { get; set; }
        public string JobBestTax { get; set; }

        public string JobXML { get; set; }
        public string ResumeXML { get; set; }
        public string JobTitle { get; set; }
        public List<string> ResumeCompetencies { get; set; }

        
    }
}