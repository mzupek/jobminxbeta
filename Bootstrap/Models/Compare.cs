﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bootstrap.Models
{
    public class Compare
    {
        public string resume { get; set; }
        public string job { get; set; }
        public Dictionary<string, string> ResumeOutput { get; set; }
        public List<string> ResumeCompetencies {get;set;}
       
        
    }
}