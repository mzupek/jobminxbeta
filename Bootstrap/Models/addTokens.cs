﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Bootstrap.Models
{
    public class addTokens
    {

        [Required(ErrorMessage = "Please Select a Plan")]
        public int plan { get; set; }
        public string email { get; set; }
        [Required(ErrorMessage = "Please enter a valid card number")]
        [StringLength(16, ErrorMessage = "This is not a valid card number, contains too many digits")]
        public string cardNumber { get; set; }
        [Required(ErrorMessage = "Please the full name that appears on your card")]
        public string cardHolderName { get; set; }
        [Required(ErrorMessage = "Exprire Month is Required")]
        public string expiryMonth { get; set; }
        [Required(ErrorMessage = "Exprire Year is Required")]
        public string expiryYear { get; set; }
        [Required(ErrorMessage = "CVV Code is Required")]
        public string cvv { get; set; }
        public string total { get; set; }
        public int upgrade { get; set; }
    }
}