﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Bootstrap.Models
{
    public class signup
    {

        [Required(ErrorMessage = "Please Select a Plan")]
        public int plan { get; set; }
        [Required(ErrorMessage ="Email Address is Required")]
        public string email { get; set; }
        [Required(ErrorMessage = "Password is Required")]
        public string password { get; set; }
        [Required(ErrorMessage = "Confirmation is Required")]
        [CompareAttribute("password", ErrorMessage = "Passwords do not match")]
        public string confirmPassword { get; set; }
        [Required]
        [CompareAttribute("email", ErrorMessage = "Emails do not match")]
        public string confirmEmail { get; set; }
        public bool offers { get; set; }
        [Required(ErrorMessage = "Please Aggree to our terms of use")]
        public bool terms { get; set; }
        
    }
}