﻿/// <reference path="member.js" />


if ($("#loaded").val() == 0)
{
    $("#resume").html("");
    $("#job").html("");
}
$("#resume_edit").tooltip();
$("#report").hide();
$("#ReportLoader").hide();
$("#ToolLoader").hide();
$("#OpLoader").hide();
$("#JobLoader").hide();
var username = $("#username").val();
$("#tab2").hide();
$("#tab3").hide();
$("#tab4").hide();
$.ajax(
       {
       type: 'GET',
       contentType: 'application/json; charset=utf-8',
       UpdateTargetId: "tokens",
       dataType: 'html',
       cache: false,
       
       url: '/Members/GetTokens',
       success: function (data) {

              var result = data;
              $('#tokens').html(result);
              var currentT = $("#currentT").val();
              var currentP = $("#currentP").val();

              if (currentT == 0 && currentP == 1) {
                  $("#RunReport").addClass("disabled");
                  
              }
       }
 });

$("#RunReport").click(function () {

    var resume = $("#resume").html();
    var job = $("#job").html();
    var rid = $("#rid").val();
   
    

    if ($("#resume").html() == "" || $("#job").html() == "") {
        $("#alerts").html("<b>Check to be sure that the Resume box and the Job Description box both contain text.</b>");
        $("#myalert").modal();
        mixpanel.track("Run Report", {
            "Details": 'Error1'

        });
    }
    else {


        $("#alerts").html("<b>Jobminx is running your report, please wait</b><span class='pull-right' style='margin-right:20px; height:50px;'><img style='height:50px;' class='img-responsive' src='https://jobminx.com/js/spiffygif_104x104.gif'/></span>");
        $("#myalert").modal();
        $("#ToolLoader").show();
        $("#RunReport").addClass('disabled');

        $.ajax(
         {
             type: 'POST',
             contentType: 'application/json; charset=utf-8',
             data: JSON.stringify({ plan: $("#currentP").val() }),
             cache: false,
            
             url: '/Members/Engage',
             success: function (data) {

             }

         });

        function activaTab(tab) {
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        };

        $.ajax(
         {
             type: 'POST',
             contentType: 'application/json; charset=utf-8',
             data: JSON.stringify({ resume: resume, job: job, rid:rid }),
             UpdateTargetId: "report",
             dataType: 'html',
             cache: false,
            
             url: '/Members/Report',
             success: function (data) {

                

                 $("#ToolLoader").hide();
                 $("#myalert").modal('hide');
                 var result = data;
                 $("#tab2").show("slow");
                 $("#tab3").show("slow");
                 $("#tab4").show("slow");
                 activaTab("ReportTab");
                 $("#ReportLoader").show();
                 $("#report").show();
                 $('#report').html(result);
                 $("#RunReport").removeClass('disabled');
                 $("#OpLoader").show();
                 mixpanel.track("Run Report", {
                     "Details": 'Success'

                 });
                 

                 $.ajax(
                        {
                            type: 'GET',
                            contentType: 'application/json; charset=utf-8',
                            UpdateTargetId: "tokens",
                            dataType: 'html',
                            cache: false,
                            url: '/Members/GetTokens',
                            success: function (data) {

                                var result = data;
                                $('#tokens').html(result);
                                currentT = $("#currentT").val();
                                var currentP = $("#currentP").val();

                                if (currentT == 0 && currentP == 1) {
                                    $("#RunReport").addClass("disabled");
                                    //$("#actionBtns").html("<a id='AddTs' href='/Members/BuyTokens' class='btn btn-primary btn-lg'>Add Tokens</button>");
                                }
                            }
                        });

             },

             error: function (error) {
                 $("#alerts").html("<b>An unexpected error has occurred (Please try pasting your content as plain text), please contact Jobminx for help - </b><a href='/Start/Contact'>Help Me!</a>");
                 $("#myalert").modal();
                 $("#ToolLoader").hide();
                 $("#RunReport").removeClass('disabled');
             }
         });
    }

});

//$("#job").focus(function () {

//    $.ajax({
//        type: 'GET',
//        contentType: 'application/json; charset=utf-8',
//        cache: false,
//        url: '/Members/JobAlert',
//        success: function (data) {

//            if(data.result == true)
//            {

//            }
//            else
//            {
//                $("#dialog2").dialog({
//                    position: { my: "center", at: "right", of: job },
//                    modal: true,
//                    buttons: {
//                        Ok: function () {

//                            $.ajax({
//                                type: 'POST',
//                                contentType: 'application/json; charset=utf-8',
//                                cache: false,
//                                url: '/Members/SetJobAlert',
//                                success: function (data) {

//                                    $("#dialog2").dialog("close");
//                                }
//                            });


                            
//                        }


//                    }
//                });
//            }
//        }
//    });

   

//});

