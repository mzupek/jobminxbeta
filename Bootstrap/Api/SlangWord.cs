﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bootstrap.Api.DTO
{
    public class SlangWord
    {
        public string SlangWordText { get; set; }
        public string SlangWordTranslation { get; set; }
    }
}