﻿using Bootstrap.Helpers;
using Bootstrap.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Bootstrap.Controllers
{
    public class SovrenController : Controller
    {
        //
        // GET: /Sovren/
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(string resume, string job, bool free, int count)
        {
            
            if (free == true)
            {
                if (count <= 0)
                {
                    // return view that says sorry please sign up
                }
            }
            //clean shit up
            Cleaner clean = new Cleaner();
            string cleanJob = clean.CleanInput(job);
            string cleanResume = clean.CleanInput(resume);

            SovrenHelper resumexml = new SovrenHelper();
            string rxml = resumexml.GetResumeXML(cleanResume);


            SovrenJob jobxml = new SovrenJob();
            string xml = jobxml.GetJobXml(cleanJob);
            Job outJob = new Job();
            Resume outResume = new Resume();

            outResume.Competencies = resumexml.GetCompetencies(rxml);
            outResume.Education = resumexml.GetEducation(rxml);
            outResume.Experience = resumexml.GetExperience(rxml);
            outResume.SingleValues = resumexml.GetResumeArray(rxml);

            outJob = jobxml.GetJobArray(xml);
            Analyze analysis = new Analyze();
            Results result = new Results();

            analysis = result.Compare(outJob, outResume);
            var jsonSerialiser = new JavaScriptSerializer();
            var json = jsonSerialiser.Serialize(analysis);
            return Json(new { result = json });

        }
	}
}