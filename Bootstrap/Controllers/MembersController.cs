﻿using Bootstrap.Helpers;
using Bootstrap.Models;
using GemBox.Document;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Net;
using HtmlAgilityPack;
using MelissaData;
using System.Data.Services.Client;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace Bootstrap.Controllers
{
    public class MembersController : Controller
    {
        // GET: Members
       
        
        public ActionResult Index()
        {
           
            if(Request["Id"] != null)
            {
                var resumeId = Request["Id"].ToString();
                ApplicationDbContext db = new ApplicationDbContext();
                var q = db.UserResumes.Find(Convert.ToInt32(resumeId));
                ViewBag.resumeText = q.Resume;
                ViewBag.name = q.Name;
                ViewBag.updated = q.DateModified.ToString();
            }
            
            string ip = Request.UserHostAddress.ToString();

            //"108.224.68.67",

            try
            {
                ipcheck ipdata = new ipcheck();
                ipobj ipob = ipdata.getIpData(ip);
                ViewBag.ZipCode = ipob.zipCode;
            }
            catch (Exception ex)
            {
                ViewBag.ZipCode = "NA";
            }
            
            if(User.Identity.IsAuthenticated)
            {
                ApplicationDbContext db = new ApplicationDbContext();
                var q = db.UserProfiles.Where(s => s.UserName == User.Identity.Name).FirstOrDefault();
                ViewBag.plan = q.Plan1;

                if (q.PlanExperation <= DateTime.Now)
                {

                    q.Plan1 = "1";
                    q.Tokens = 5;
                    q.PlanExperation = DateTime.Now.AddMonths(1);
                    db.SaveChanges();

                }
            }
            else
            {

               //user is good to use unlimited

            }
            
            
            

            return View();
        }
        [HttpGet]
        public ActionResult JobAlert()
        {
            if(User.Identity.IsAuthenticated)
            {
                ApplicationDbContext db = new ApplicationDbContext();
                var q = db.UserProfiles.Where(e => e.UserName == User.Identity.Name).SingleOrDefault();
                if(q.JobAlert)
                {
                    return Json(new { result = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { result = false }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json(new { result = false }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public ActionResult SetJobAlert()
        {
            if (User.Identity.IsAuthenticated)
            {
                ApplicationDbContext db = new ApplicationDbContext();
                var q = db.UserProfiles.Where(e => e.UserName == User.Identity.Name).SingleOrDefault();
                q.JobAlert = true;
                db.SaveChanges();
                return Json(new { result = true });
            }
            else
            {
                return Json(new { result = false });
            }


        }

        public ActionResult DeleteResumeConfirm()
        {
            var resumeId = Request["Id"].ToString();
            ViewBag.Id = resumeId;
            return View();
        }

        public ActionResult DeleteResume()
        {

            var resumeId = Request["Id"].ToString();
            ApplicationDbContext db = new ApplicationDbContext();
            var q = db.UserResumes.Find(Convert.ToInt32(resumeId));
            db.UserResumes.Remove(q);
            db.SaveChanges();
            return RedirectToAction("UserProfile");
        }

        public ActionResult Upgrade()
        {

            return View();
        }
      
        [HttpPost]
        public ActionResult Engage(string plan)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            ApplicationDbContext.Engagement ngage = new ApplicationDbContext.Engagement();
            ngage.Plan = plan;
            if(!User.Identity.IsAuthenticated)
            {
                ngage.UserId = "Unkown";
            }
            else
            {
                ngage.UserId = User.Identity.Name;
            }
            
            ngage.Expiration = DateTime.Now;
            db.Engagements.Add(ngage);
            db.SaveChanges();

            return Json(new { result = "added!" });
        }
        
        [HttpPost]
        public ActionResult SaveResume(string text, string rid)
        {
            var nrid = Convert.ToInt32(rid);
            ApplicationDbContext db = new ApplicationDbContext();
            if(nrid != 0 )
            {
                var q = db.UserResumes.Find(nrid);
                q.Resume = text;
                db.SaveChanges();

                return Json(new { result = "success" });
            }
            else
            {
                return Json(new { result = "fail" });
            }

        }

        [HttpPost]
        public ActionResult SaveResumePaste(string text, string zip, string name)
        {
            
            ApplicationDbContext db = new ApplicationDbContext();

            ApplicationDbContext.UserResume u = new ApplicationDbContext.UserResume();
            u.Name = name;
            u.UserId = User.Identity.Name;
            u.Resume = text;
            u.DateModified = DateTime.Now;
            u.DateLoaded = DateTime.Now;
            u.ZipCode = zip;
            u.ContentType = "application/pdf";
            db.UserResumes.Add(u);    
            db.SaveChanges();

            return Json(new { result = "success", id= u.Id });
            
        }

        [HttpPost]
        public ActionResult SaveResumeAs(int id, string name, string zip, string text)
        {

            ApplicationDbContext db = new ApplicationDbContext();
            var or = db.UserResumes.Find(id);

            ApplicationDbContext.UserResume nr = new ApplicationDbContext.UserResume();
            nr.Name = name;
            nr.UserId = User.Identity.Name;
            nr.Resume = text;
            nr.DateModified = DateTime.Now;
            nr.DateLoaded = DateTime.Now;
            nr.ZipCode = zip;
            nr.ContentType = "application/pdf";
            db.UserResumes.Add(nr);
            db.SaveChanges();

            return Json(new { result = "success", id = nr.Id });

        }
        
        public ActionResult GetTokens()
        {
            

            if (!User.Identity.IsAuthenticated)
            {
                FreeTrials ft = new FreeTrials();
                int ftl = ft.TriesLeft(Request.UserHostAddress);
                if (ftl == 0)
                {
                    ViewData["tokens"] = 0;
                    ViewData["plan"] = 1;
                    return PartialView("_GetTokensNon");
                }
                else
                {
                    ViewData["tokens"] = 1;
                    ViewData["plan"] = 1;
                    return PartialView("_GetTokensNon");
                }
            }
            else
            {
                string username = User.Identity.Name;
                ApplicationDbContext db = new ApplicationDbContext();
                var up = db.UserProfiles.Where(m => m.UserName == username).FirstOrDefault();
                var a = db.ActionItems.Where(m => m.UserId == up.UserId).FirstOrDefault();
                ViewBag.firstname = up.FirstName;
                ViewData["expire"] = up.PlanExperation;
                ViewData["plan"] = up.Plan1;
                if (up.Tokens <= 0)
                {
                    up.Tokens = 0;
                }
                if (up.Plan1 == "3" || up.Plan1 == "2")
                {
                    ViewData["tokens"] = 0;
                }
                else
                {
                    ViewData["tokens"] = up.Tokens;
                }

                return PartialView("_GetTokens");
            }
        }

      
        [HttpPost]   
        public ActionResult Report(string resume, string job, int rid)
        {
            if (!User.Identity.IsAuthenticated)
            {

                ApplicationDbContext db = new ApplicationDbContext();
                FreeTrials ft = new FreeTrials();
                int ftl = ft.TriesLeft(Request.UserHostAddress);
                if (ftl == 0)
                {
                    return RedirectToAction("Redirect");
                }
                else
                {

                    string cleanresume = HtmlSanitizer.Sanitize(resume);
                    string cleanjob = HtmlSanitizer.Sanitize(job);


                    SovrenHelper resumexml = new SovrenHelper();
                    string rxml = resumexml.GetResumeXML(cleanresume);


                    SovrenJob jobxml = new SovrenJob();
                    string xml = jobxml.GetJobXml(cleanjob);

                    Job outJob = new Job();
                    Resume outResume = new Resume();

                    outResume.Competencies = resumexml.GetCompetencies(rxml);
                    outResume.Education = resumexml.GetEducation(rxml);
                    outResume.Experience = resumexml.GetExperience(rxml);
                    outResume.SingleValues = resumexml.GetResumeArray(rxml);
                    outResume.Taxonomies = resumexml.GetBestFitTaxonomies(rxml);
                    outResume.BestTax = outResume.Taxonomies.FirstOrDefault();
                    outResume.ResumeText = resume;

                    outJob = jobxml.GetJobArray(xml);
                    outJob.JobText = job;
                    string jt = outJob.SingleValues["JobTitle"];

                    Analyze analysis = new Analyze();
                    Results result = new Results();

                    analysis = result.Compare(outJob, outResume);
                    analysis.JobRaw = job;
                    //clean resume good//

                    HtmlUtility hcleaner = new HtmlUtility();
                    analysis.ResumeRaw = resume;
                    analysis.JobBestTax = outJob.BestTax;
                    analysis.JobTitle = jt;
                    

                    
                    analysis.JobXML = xml;
                    analysis.ResumeXML = rxml;
                    
                    ft.IncrementTries(Request.UserHostAddress.ToString());

                    var ru = db.UserResumes.Where(z => z.UserId == User.Identity.Name && z.Id == rid).SingleOrDefault();
                    if (ru != null)
                    {
                        foreach (var item in outResume.Competencies)
                        {
                            ru.Skills += item + ",";
                        }
                        ru.Summary = analysis.QualificationSummary;
                        ru.Experience = analysis.ResumeYOE.ToString();
                        ru.Education = analysis.ResumeEducation;
                        ru.Category = analysis.ResumeBestTax;
                        db.SaveChanges();
                    }

                    return PartialView("_PartialReport", analysis);

                }


            }
            else
            {

                Cleaner clean = new Cleaner();
                //string cleanJob = clean.CleanInput(job);
                //string cleanResume = clean.CleanInput(resume);

                SovrenHelper resumexml = new SovrenHelper();
                string rxml = resumexml.GetResumeXML(resume);


                SovrenJob jobxml = new SovrenJob();
                string xml = jobxml.GetJobXml(job);

                Job outJob = new Job();
                Resume outResume = new Resume();

                outResume.Competencies = resumexml.GetCompetencies(rxml);
                outResume.Education = resumexml.GetEducation(rxml);
                outResume.Experience = resumexml.GetExperience(rxml);
                outResume.SingleValues = resumexml.GetResumeArray(rxml);
                outResume.Taxonomies = resumexml.GetBestFitTaxonomies(rxml);
                outResume.BestTax = outResume.Taxonomies.FirstOrDefault();
                outResume.ResumeText = resume;

                outJob = jobxml.GetJobArray(xml);
                outJob.JobText = job;
                string jt = outJob.SingleValues["JobTitle"];

                Analyze analysis = new Analyze();
                Results result = new Results();

                analysis = result.Compare(outJob, outResume);
                analysis.JobRaw = job;
                //clean resume good//

                HtmlUtility hcleaner = new HtmlUtility();
                analysis.ResumeRaw = resume;
                analysis.JobBestTax = outJob.BestTax;
                analysis.JobTitle = jt;
                ApplicationDbContext db = new ApplicationDbContext();
                var up = db.UserProfiles.Where(m => m.UserName == User.Identity.Name).FirstOrDefault();
                if (up.Plan1 == "3" || up.Plan1 == "2")
                {
                    //Unlimited no reason to count down unlimited
                }
                else
                {
                    if (up.Tokens <= 0)
                    {
                        up.Tokens = 0;
                    }
                    else
                    {
                        up.Tokens = up.Tokens - 1;
                    }
                }

                analysis.JobXML = xml;
                analysis.ResumeXML = rxml;
                db.SaveChanges();
                var ru = db.UserResumes.Where(z => z.UserId == User.Identity.Name && z.Id == rid).SingleOrDefault();
                if (ru != null)
                {
                    foreach (var item in outResume.Competencies)
                    {
                        ru.Skills += item + ",";
                    }
                    ru.Summary = analysis.QualificationSummary;
                    ru.Experience = analysis.ResumeYOE.ToString();
                    ru.Education = analysis.ResumeEducation;
                    ru.Category = analysis.ResumeBestTax;
                    db.SaveChanges();
                }
                return PartialView("_PartialReport", analysis);
            }
        }

       
        [HttpPost]
        public ActionResult GetJobs(string search)
        {

            SimplyHired sh = new SimplyHired();
            List<SimplyHiredJob> jobresult = sh.getJobs(search, Request.UserHostAddress.ToString());

            return PartialView("_PartialJobRecs", jobresult);

        }
       
        public ActionResult BuyTokens()
        {


            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BuyTokens(addTokens model, FormCollection form)
        {
            if (ModelState.IsValid)
            {
                Merchant trans = new Merchant();

                PurchaseSummary transaction = new PurchaseSummary();
                string exdate = model.expiryMonth + model.expiryYear;
                MerchantResponse result = trans.charge(model.cardHolderName, model.cardNumber, exdate, model.total);
                IdentityResult userresult = new IdentityResult();
                ApplicationUser user = new ApplicationUser();
                
                if (result.IsTransactionApproved)
                {
                    string[] plans = { "NA", "Starter", "Plus", "Unlimited" };
                    ApplicationDbContext db = new ApplicationDbContext();
                    
                    using (var dbContextTransaction = db.Database.BeginTransaction())
                    {

                        try
                        {
                            
                            

                            ApplicationDbContext.UserProfile up = new ApplicationDbContext.UserProfile();
                            up = db.UserProfiles.Where(m => m.UserName == model.email).FirstOrDefault();

                           
                            up.IpAddress = HttpContext.Request.UserHostAddress;
                            up.CreatedDate = DateTime.Now;
                            up.PlanExperation = DateTime.Now.AddMonths(1);


                            int toks = 0;
                            if (up.Plan1 == "1")
                            {
                                toks = 5;
                            }
                            if (up.Plan1 == "2")
                            {
                                toks = 25;
                            }
                            if (up.Plan1 == "3")
                            {
                                toks = 0;
                            }
                            up.Tokens = up.Tokens + toks;
                            up.Plan1 = model.plan.ToString();

                            ApplicationDbContext.ActionItem a = new ApplicationDbContext.ActionItem();
                            a.UserName = up.UserName;
                            a.UserId = up.UserId;

                            a.Expiration = Convert.ToDateTime(model.expiryMonth + "/" + model.expiryYear);
                            a.Worth = result.ammount;
                            a.UserName = up.UserName;
                            
                            a.PromoId = Convert.ToInt32(up.Plan1);

                            a.UserId = up.UserId;
                            a.PurchaseDate = DateTime.Now;
                            a.Expiration = DateTime.Now.AddMonths(1);
                            a.TransactionId = result.authNum;
                            a.ctr = result.ctr;

                            db.ActionItems.Add(a);

                            transaction.username = up.UserName;
                            transaction.email = up.EmailAddress;
                            transaction.orderNumber = result.authNum;
                            transaction.plan = plans[Convert.ToInt32(up.Plan1)];
                            transaction.ctr = result.ctr;
                            transaction.exDate = result.exDate;
                            transaction.tokens = toks;
                            transaction.total = result.ammount;
                            transaction.tokenSummary = "Tokens Purchased: " + toks;
                            
                            db.SaveChanges();
                            dbContextTransaction.Commit();
                            Messenger emailit = new Messenger();
                            string smtpresponse = emailit.SendWelcomeMessage(transaction, model.cardHolderName);
                            return PartialView("_PartialOrderSummary", transaction);
                            
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            ViewBag.PurchaseError = "Something went wrong, check the information you entered and try again." + "(" + ex.Message + ")";
                            return PartialView("_PartialBuyTokens", model);
                        }
                    }


                }
                else
                {
                    ViewBag.PurchaseError = "Something went wrong, check the information you entered and try again." + "(" + result.Message + ")";
                    return PartialView("_PartialBuyTokens", model);
                }

            }
            return PartialView("_PartialBuyTokens", model);
            //return Content(result.IsTransactionApproved.ToString() + "<br/><br/>" + result.Message + "<br/><br/>" + result.authNum +"<br/><br/>"+ result.ctr +"<br/><br/>"+ result.ammount);


        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Clean(string html)
        {
            //HtmlUtility hcleaner = new HtmlUtility();
            //string clean = hcleaner.SanitizeHtml(html);
            //HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            //doc.LoadHtml(html);

            //doc.DocumentNode.Descendants()
            //                .Where(n => n.Name == "script" || n.Name == "style")
            //                .ToList()
            //                .ForEach(n => n.Remove());
            //string results = doc.DocumentNode.OuterHtml;
            
            string clean = HtmlSanitizer.Sanitize(html);
            return Json(new{clean=clean});


        }
        
      
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Upgrade(addTokens model, FormCollection form)
        {
           
            ApplicationDbContext db = new ApplicationDbContext();
                var up = db.UserProfiles.Where(m => m.UserName == model.email).FirstOrDefault();
            if (ModelState.IsValid)
            {
                
                Merchant trans = new Merchant();
                
                ViewBag.plan = up.Plan1;
                PurchaseSummary transaction = new PurchaseSummary();
                string exdate = model.expiryMonth + model.expiryYear;
                MerchantResponse result = trans.charge(model.cardHolderName, model.cardNumber, exdate, model.total);
                IdentityResult userresult = new IdentityResult();
                ApplicationUser user = new ApplicationUser();

                if (result.IsTransactionApproved)
                {

                    string[] plans = { "NA", "Starter", "Plus", "Unlimited" };
                    

                    using (var dbContextTransaction = db.Database.BeginTransaction())
                    {

                        try
                        {
                           
                            up.Plan1 = "3";
                            up.IpAddress = HttpContext.Request.UserHostAddress;
                            up.CreatedDate = DateTime.Now;
                            up.PlanExperation = DateTime.Now.AddMonths(1);
                            up.Tokens = 0;

                            

                            ApplicationDbContext.ActionItem a = new ApplicationDbContext.ActionItem();
                            a.UserName = up.UserName;
                            a.UserId = up.UserId;

                            a.Expiration = Convert.ToDateTime(model.expiryMonth + "/" + model.expiryYear);
                            a.Worth = result.ammount;
                            a.UserName = up.UserName;

                            a.PromoId = 4;

                            a.UserId = up.UserId;
                            a.PurchaseDate = DateTime.Now;
                            a.Expiration = DateTime.Now.AddMonths(1);
                            a.TransactionId = result.authNum;
                            a.ctr = result.ctr;

                            db.ActionItems.Add(a);

                            transaction.username = up.UserName;
                            transaction.email = up.EmailAddress;
                            transaction.orderNumber = result.authNum;
                            transaction.plan = plans[Convert.ToInt32(up.Plan1)];
                            transaction.ctr = result.ctr;
                            transaction.exDate = result.exDate;
                            transaction.tokens = 0;
                            transaction.total = result.ammount;
                            transaction.tokenSummary = "You have Unlimited uses until " + up.PlanExperation.ToString();

                            db.SaveChanges();

                            dbContextTransaction.Commit();
                            Messenger emailit = new Messenger();
                            string smtpresponse = emailit.SendWelcomeMessage(transaction, model.cardHolderName);
                            
                            return PartialView("_PartialOrderSummary", transaction);


                        }
                        catch (Exception ex)
                        {

                            
                            dbContextTransaction.Rollback();
                            ViewBag.PurchaseError = "Something went wrong, check the information you entered and try again." + "(" + ex.Message + ")";
                            ViewBag.Plan = up.Plan1;
                            return PartialView("_PartialUpgrade", model);
                            //return Json(new {status = "error", message = "Something went wrong, check the information you entered and try again." + "(" + ex.Message + ")"});
    
                            
    
                        }

                    } 



                }
                else
                {
                   
                    ViewBag.PurchaseError = "Something went wrong, check the information you entered and try again." + "(" + result.Message + ")";
                    ViewBag.Plan = up.Plan1;
                    return PartialView("_PartialUpgrade", model);
                    //return Json(new { status = "error", message = "Something went wrong, check the information you entered and try again." });
                }

            }
            ViewBag.Plan = up.Plan1;
            return PartialView("_PartialUpgrade", model);
            //return Content(result.IsTransactionApproved.ToString() + "<br/><br/>" + result.Message + "<br/><br/>" + result.authNum +"<br/><br/>"+ result.ctr +"<br/><br/>"+ result.ammount);
            //return Json(new { status = "error", message = "Something went wrong, check the information you entered and try again." });

        }

        
        public ActionResult UserProfile()
        {


            ApplicationDbContext db = new ApplicationDbContext();
            var up = db.UserProfiles.Where(m => m.UserName == User.Identity.Name).FirstOrDefault();


            return View(up);

        }

        [HttpPost]
        public ActionResult UploadDocument()
        {
            
            if (Request.Files.AllKeys.Any())
            {
                // Get the uploaded image from the Files collection
                var httpPostedFile = Request.Files["UploadedFile"];
                var zip = Request.Form["zip"];
                string finaltext = "";
                if (httpPostedFile != null)
                {
                    ComponentInfo.SetLicense("DBTX-8EIK-50LW-QTKQ");

                    if (httpPostedFile.ContentType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                    {
                        DocumentModel document = DocumentModel.Load(httpPostedFile.InputStream, LoadOptions.DocxDefault);
                        StringBuilder sb = new StringBuilder();

                        foreach (GemBox.Document.Paragraph paragraph in document.GetChildElements(true, ElementType.Paragraph))
                        {
                            foreach (Run run in paragraph.GetChildElements(true, ElementType.Run))
                            {
                                bool isBold = run.CharacterFormat.Bold;
                                string text = run.Text;

                                sb.AppendFormat("{0}{1}{2}", isBold ? "<b>" : "", text, isBold ? "</b>" : "");
                            }
                            sb.AppendLine();
                        }
                        
                        finaltext = sb.ToString();
                        finaltext = finaltext.Replace("\n", "<br/>");
                    }

                    if (httpPostedFile.ContentType == "application/msword")
                    {
                        
                        DocumentModel document = DocumentModel.Load(httpPostedFile.InputStream, LoadOptions.DocDefault);
                        StringBuilder sb = new StringBuilder();

                        foreach (GemBox.Document.Paragraph paragraph in document.GetChildElements(true, ElementType.Paragraph))
                        {
                            foreach (Run run in paragraph.GetChildElements(true, ElementType.Run))
                            {
                                bool isBold = run.CharacterFormat.Bold;
                                string text = run.Text;

                                sb.AppendFormat("{0}{1}{2}", isBold ? "<b>" : "", text, isBold ? "</b>" : "");
                            }
                            sb.AppendLine();
                        }
                        
                        finaltext = sb.ToString();
                        finaltext = finaltext.Replace("\n", "<br/>");
                    }

                    if (httpPostedFile.ContentType == "application/pdf")
                    {
                        string currentText = "";
                        PdfReader reader = new PdfReader(httpPostedFile.InputStream);
                        
                        for (int page = 1; page <= reader.NumberOfPages; page++)
                        {
                            ITextExtractionStrategy strategy = new  SimpleTextExtractionStrategy();
                            currentText += PdfTextExtractor.GetTextFromPage(reader, page, strategy);

                            
                            
                           
                        }
                        currentText = Encoding.UTF8.GetString(ASCIIEncoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(currentText)));
                        finaltext = currentText.Replace("\n", "<br/>");

                    }

                    if (httpPostedFile.ContentType == "text/plain")
                    {
                        DocumentModel document = DocumentModel.Load(httpPostedFile.InputStream, LoadOptions.TxtDefault);

                        StringBuilder sb = new StringBuilder();

                        foreach (GemBox.Document.Paragraph paragraph in document.GetChildElements(true, ElementType.Paragraph))
                        {
                            foreach (Run run in paragraph.GetChildElements(true, ElementType.Run))
                            {
                                bool isBold = run.CharacterFormat.Bold;
                                string text = run.Text;

                                sb.AppendFormat("{0}{1}{2}", isBold ? "<b>" : "", text, isBold ? "</b>" : "");
                            }
                            sb.AppendLine();
                        }
                        finaltext = sb.ToString();
                    }
                    ApplicationDbContext db = new ApplicationDbContext();
                    ApplicationDbContext.UserResume ur = new ApplicationDbContext.UserResume();
                    if(!User.Identity.IsAuthenticated)
                    {
                        ur.UserId = "Unkown";
                    }
                    else
                    {
                        ur.UserId = User.Identity.Name;
                    }
                    
                    ur.Resume = finaltext;
                    ur.Name = httpPostedFile.FileName;
                    ur.ContentType = httpPostedFile.ContentType;
                    ur.DateLoaded = DateTime.Now;
                    ur.DateModified = DateTime.Now;
                    ur.ZipCode = zip;
                    db.UserResumes.Add(ur);
                    db.SaveChanges();

                    //Console.WriteLine(sb.ToString());
                    return Json(new { resume = finaltext, name = ur.Name, id = ur.Id, lm = ur.DateModified.ToString(), created = ur.DateLoaded.ToString() });
                }
            }

            return Json(new { text = "Error" });
        }
        
       
        [HttpPost]
        public ActionResult UploadFromDropBox(string Url, string name, string zip)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            string finaltext = "";
            //Create a stream for the file
            Stream stream = null;

            //This controls how many bytes to read at a time and send to the client
            int bytesToRead = 10000;

            // Buffer to read bytes in chunk size specified above
            byte[] buffer = new Byte[bytesToRead];

            // The number of bytes read
            try
            {
                //Create a WebRequest to get the file
                HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(Url);

                //Create a response for this request
                HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

                if (fileReq.ContentLength > 0)
                    fileResp.ContentLength = fileReq.ContentLength;

                //Get the Stream returned from the response
                stream = fileResp.GetResponseStream();
                MemoryStream ms = new MemoryStream();
                stream.CopyTo(ms);
                // prepare the response to the client. resp is the client Response
                var resp = Response;

                //Indicate the type of data being sent
                resp.ContentType = "application/octet-stream";
                string fileName = "Resume";
                //Name the file 
                resp.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());
               
                int length;
                do
                {
                    // Verify that the client is connected.
                    if (resp.IsClientConnected)
                    {
                        // Read data into the buffer.
                        length = stream.Read(buffer, 0, bytesToRead);

                        // and write it out to the response's output stream
                        resp.OutputStream.Write(buffer, 0, length);

                       
                        if (resp != null)
                        {
                            ComponentInfo.SetLicense("DBTX-8EIK-50LW-QTKQ");

                            if (fileResp.ContentType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                            {
                                DocumentModel document = DocumentModel.Load(ms, LoadOptions.DocxDefault);
                                StringBuilder sb = new StringBuilder();

                                foreach (GemBox.Document.Paragraph paragraph in document.GetChildElements(true, ElementType.Paragraph))
                                {
                                    foreach (Run run in paragraph.GetChildElements(true, ElementType.Run))
                                    {
                                        bool isBold = run.CharacterFormat.Bold;
                                        string text = run.Text;

                                        sb.AppendFormat("{0}{1}{2}", isBold ? "<b>" : "", text, isBold ? "</b>" : "");
                                    }
                                    sb.AppendLine();
                                }

                                finaltext = sb.ToString();
                                finaltext = finaltext.Replace("\n", "<br/>");
                                
                                ApplicationDbContext.UserResume ur = new ApplicationDbContext.UserResume();
                                if (!User.Identity.IsAuthenticated)
                                {
                                    ur.UserId = "Unkown";
                                }
                                else
                                {
                                    ur.UserId = User.Identity.Name;
                                }
                                ur.Resume = finaltext;
                                ur.Name = name;
                                ur.ContentType = fileResp.ContentType;
                                ur.DateLoaded = DateTime.Now;
                                ur.DateModified = DateTime.Now;
                                ur.ZipCode = zip;
                                db.UserResumes.Add(ur);
                                db.SaveChanges();
                                return Json(new { resume = finaltext, name = ur.Name, id = ur.Id, lm = ur.DateModified.ToString(), created = ur.DateLoaded.ToString() });
                            }

                            if (fileResp.ContentType == "application/msword")
                            {

                                DocumentModel document = DocumentModel.Load(ms, LoadOptions.DocDefault);
                                StringBuilder sb = new StringBuilder();

                                foreach (GemBox.Document.Paragraph paragraph in document.GetChildElements(true, ElementType.Paragraph))
                                {
                                    foreach (Run run in paragraph.GetChildElements(true, ElementType.Run))
                                    {
                                        bool isBold = run.CharacterFormat.Bold;
                                        string text = run.Text;

                                        sb.AppendFormat("{0}{1}{2}", isBold ? "<b>" : "", text, isBold ? "</b>" : "");
                                    }
                                    sb.AppendLine();
                                }

                                finaltext = sb.ToString();
                                finaltext = finaltext.Replace("\n", "<br/>");
                                ApplicationDbContext.UserResume ur = new ApplicationDbContext.UserResume();
                                if (!User.Identity.IsAuthenticated)
                                {
                                    ur.UserId = "Unkown";
                                }
                                else
                                {
                                    ur.UserId = User.Identity.Name;
                                }
                                ur.Resume = finaltext;
                                ur.Name = name;
                                ur.ContentType = fileResp.ContentType;
                                ur.DateLoaded = DateTime.Now;
                                ur.DateModified = DateTime.Now;
                                ur.ZipCode = zip;
                                db.UserResumes.Add(ur);
                                db.SaveChanges();
                                return Json(new { resume = finaltext, name = ur.Name, id = ur.Id, lm = ur.DateModified.ToString(), created = ur.DateLoaded.ToString() });
                            }

                            if (fileResp.ContentType == "application/pdf")
                            {
                                string currentText = "";
                                PdfReader reader = new PdfReader(ms.ToArray());
                                for (int page = 1; page <= reader.NumberOfPages; page++)
                                {
                                    ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                                    currentText += PdfTextExtractor.GetTextFromPage(reader, page, strategy);

                                    
                                    
                                }
                                
                                currentText = Encoding.UTF8.GetString(ASCIIEncoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(currentText)));
                                finaltext = currentText.Replace("\n", "<br/>");
                                ApplicationDbContext.UserResume ur = new ApplicationDbContext.UserResume();
                                if (!User.Identity.IsAuthenticated)
                                {
                                    ur.UserId = "Unkown";
                                }
                                else
                                {
                                    ur.UserId = User.Identity.Name;
                                }
                                ur.Resume = finaltext;
                                ur.Name = name;
                                ur.ContentType = fileResp.ContentType;
                                ur.DateLoaded = DateTime.Now;
                                ur.DateModified = DateTime.Now;
                                ur.ZipCode = zip;
                                db.UserResumes.Add(ur);
                                db.SaveChanges();
                                return Json(new { resume = finaltext, name = ur.Name, id = ur.Id, lm = ur.DateModified.ToString(), created = ur.DateLoaded.ToString() });

                            }

                            if (fileResp.ContentType == "text/plain")
                            {
                                DocumentModel document = DocumentModel.Load(ms, LoadOptions.TxtDefault);

                                StringBuilder sb = new StringBuilder();

                                foreach (GemBox.Document.Paragraph paragraph in document.GetChildElements(true, ElementType.Paragraph))
                                {
                                    foreach (Run run in paragraph.GetChildElements(true, ElementType.Run))
                                    {
                                        bool isBold = run.CharacterFormat.Bold;
                                        string text = run.Text;

                                        sb.AppendFormat("{0}{1}{2}", isBold ? "<b>" : "", text, isBold ? "</b>" : "");
                                    }
                                    sb.AppendLine();
                                }
                                finaltext = sb.ToString();
                                ApplicationDbContext.UserResume ur = new ApplicationDbContext.UserResume();
                                if (!User.Identity.IsAuthenticated)
                                {
                                    ur.UserId = "Unkown";
                                }
                                else
                                {
                                    ur.UserId = User.Identity.Name;
                                }
                                ur.Resume = finaltext;
                                ur.Name = name;
                                ur.ContentType = fileResp.ContentType;
                                ur.DateLoaded = DateTime.Now;
                                ur.DateModified = DateTime.Now;
                                ur.ZipCode = zip;
                                db.UserResumes.Add(ur);
                                db.SaveChanges();
                                return Json(new { resume = finaltext, name = ur.Name, id = ur.Id, lm = ur.DateModified.ToString(), created = ur.DateLoaded.ToString() });
                            }
                            
                            // Flush the data
                            resp.Flush();

                            //Clear the buffer
                            buffer = new Byte[bytesToRead];
                           
                        }
                    }
                    else
                    {
                        // cancel the download if client has disconnected
                        length = -1;
                    }
                } while (length > 0); //Repeat until no data is read
            }
              
            finally
            {
                if (stream != null)
                {
                    
                    //Close the input stream
                    stream.Close();
                }


            }

            return Json(new { resume = "Something went wrong please try again later." });
        }
    
        public ActionResult Resumes()
        {

            ApplicationDbContext db = new ApplicationDbContext();
            var q = db.UserResumes.Where(e => e.UserId == User.Identity.Name).ToList().OrderByDescending(d=>d.DateLoaded);
            return PartialView("_PartialResumes", q);
        }

        [HttpPost]
        public ActionResult GetJobFromIndeed(string url)
        {


            HttpWebRequest web_request = (HttpWebRequest)WebRequest.Create(url);
            var response_string = "";
            using (HttpWebResponse web_response = (HttpWebResponse)web_request.GetResponse())
            {
                using (StreamReader response_stream = new StreamReader(web_response.GetResponseStream()))
                {
                    response_string = response_stream.ReadToEnd();
                }
            }

            HtmlDocument doc = new HtmlDocument();

            doc.LoadHtml(response_string);

            var jobdesc = doc.GetElementbyId("job_summary").InnerHtml;
            //split by Compensation:???//
            var jobtitle = doc.GetElementbyId("job_header").InnerHtml;

            return Json(new { jobdesc = jobdesc, jobtitle = jobtitle });
        }

        [HttpPost]
        public ActionResult SearchIndeed(string search, string loc)
        {
            HttpWebRequest web_request = (HttpWebRequest)WebRequest.Create("http://api.indeed.com/ads/apisearch?publisher=5422421003768593&q="+search+"&l="+loc+"&sort=&radius=&st=&jt=&format=json&start=&limit=&fromage=&filter=&latlong=1&co=us&chnl=&userip=&useragent=&v=2");
            web_request.Method = "GET";
            web_request.ContentType = "application/json; charset=utf-8";
            var response_string = "";
            using (HttpWebResponse web_response = (HttpWebResponse)web_request.GetResponse())
            {
                using (StreamReader response_stream = new StreamReader(web_response.GetResponseStream()))
                {
                    response_string = response_stream.ReadToEnd();
                }
            }

            return Json(new { response_string });
        }
        
        [HttpPost]
        [ValidateInput(false)]
        public FileStreamResult CreatePDFReport(Analyze analysis, string Mkeywords1, string Nkeywords1)
        {


           MemoryStream fs = new MemoryStream();
           Document document = new Document(PageSize.A4, 30, 30, 30, 30);
           PdfWriter writer = PdfWriter.GetInstance(document, fs);
           //writer.CloseStream = false;

            document.Open();


            iTextSharp.text.Image pic = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/jobminx_logo_final_small.png"));

            pic.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;
            
            

            document.Add(pic);
            

            PdfContentByte cb = writer.DirectContent;

            BaseFont myfont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.NOT_EMBEDDED);
            
           

            
            iTextSharp.text.Paragraph title = new iTextSharp.text.Paragraph("Keyword Density Report -" + DateTime.Now.ToShortDateString(), FontFactory.GetFont("Verdana", 16));

            title.SpacingAfter = 10;

            

            iTextSharp.text.Paragraph ReportOverviewTitle = new iTextSharp.text.Paragraph("Report Overview", FontFactory.GetFont("Verdana", 14));

            ReportOverviewTitle.SpacingAfter = 10;

            iTextSharp.text.Paragraph ai = new iTextSharp.text.Paragraph("Jobminx has identified "+analysis.KeyWordCountJob+" Keywords to target this job description. Your resume currently has "+analysis.KeyWordMacthCount+" matching keywords", FontFactory.GetFont("Verdana", 14));

            ai.SpacingAfter = 10;

            iTextSharp.text.Paragraph Summary = new iTextSharp.text.Paragraph("Summary", FontFactory.GetFont("Verdana", 12));
            
            Summary.SpacingAfter = 10;
            
            iTextSharp.text.Paragraph Summary_text = new iTextSharp.text.Paragraph(analysis.QualificationSummary, FontFactory.GetFont("Verdana", 10));

            Summary_text.SpacingAfter = 10;

            //Keywords//

            iTextSharp.text.Paragraph kwd = new iTextSharp.text.Paragraph("Keyword Density", FontFactory.GetFont("Verdana", 12));
            
            kwd.SpacingAfter = 10;
            
            iTextSharp.text.Paragraph jd_text = new iTextSharp.text.Paragraph("Number of Keywords in Job Description: " + analysis.KeyWordCountJob.ToString(), FontFactory.GetFont("Verdana", 10));

            jd_text.SpacingAfter = 5;

            iTextSharp.text.Paragraph km_text = new iTextSharp.text.Paragraph("Number of Keywords Matched: " + analysis.KeyWordMacthCount.ToString(), FontFactory.GetFont("Verdana", 10));

            km_text.SpacingAfter = 10;

            iTextSharp.text.Paragraph matching = new iTextSharp.text.Paragraph("Matching Keywords", FontFactory.GetFont("Verdana", 12));

            matching.SpacingAfter = 5;

            iTextSharp.text.Paragraph matchingkw = new iTextSharp.text.Paragraph(Mkeywords1, FontFactory.GetFont("Verdana", 10));

            matchingkw.SpacingAfter = 5;
          

            iTextSharp.text.Paragraph notmatching = new iTextSharp.text.Paragraph("Not Matching Keywords", FontFactory.GetFont("Verdana", 12));

            notmatching.SpacingAfter = 5;

            iTextSharp.text.Paragraph matchingnkw = new iTextSharp.text.Paragraph(Nkeywords1, FontFactory.GetFont("Verdana", 10));

            matchingnkw.SpacingAfter = 5;

            

            ///Education///

            iTextSharp.text.Paragraph jobEDwords = new iTextSharp.text.Paragraph("Job Education", FontFactory.GetFont("Verdana", 12));

            jobEDwords.SpacingAfter = 5;
            
            iTextSharp.text.Paragraph jobED = new iTextSharp.text.Paragraph(analysis.JobEducation, FontFactory.GetFont("Verdana", 10));

            jobED.SpacingAfter = 10;

            iTextSharp.text.Paragraph resumeEDwords = new iTextSharp.text.Paragraph("Resume Education", FontFactory.GetFont("Verdana", 12));

            resumeEDwords.SpacingAfter = 5;

            iTextSharp.text.Paragraph resumeED = new iTextSharp.text.Paragraph(analysis.ResumeEducation, FontFactory.GetFont("Verdana", 10));

            resumeED.SpacingAfter = 10;

            //YOE

            iTextSharp.text.Paragraph jobYOEwords = new iTextSharp.text.Paragraph("Job Experience: " + analysis.JobYOE.ToString(), FontFactory.GetFont("Verdana", 10));

            jobYOEwords.SpacingAfter = 5;

            

            iTextSharp.text.Paragraph resumeYOEwords = new iTextSharp.text.Paragraph("Resume Experience: " + analysis.ResumeYOE.ToString(), FontFactory.GetFont("Verdana", 10));

            resumeYOEwords.SpacingAfter = 10;

            
            


            //string cleanresume = Regex.Replace(analysis.ResumeRaw, "<.*?>", string.Empty);
            iTextSharp.text.Paragraph resume = new iTextSharp.text.Paragraph("Your Resume", FontFactory.GetFont("Verdana", 14));

            

            iTextSharp.text.Paragraph resume_text = new iTextSharp.text.Paragraph(analysis.ResumeRaw, FontFactory.GetFont("Verdana", 10));

            resume_text.SpacingAfter = 10;

            iTextSharp.text.Paragraph job = new iTextSharp.text.Paragraph("Job Description", FontFactory.GetFont("Verdana", 14));

            
            
            //string cleanjob = Regex.Replace(analysis.JobRaw, "<.*?>", string.Empty);
            //Cleaner cl = new Cleaner();
            //string cleanerjob = cl.CleanInput(cleanjob);
            iTextSharp.text.Paragraph job_text = new iTextSharp.text.Paragraph(analysis.JobRaw, FontFactory.GetFont("Verdana", 10));

            job_text.SpacingAfter = 10;

            document.Add(title);
            document.Add(Summary);
            document.Add(ai);
            document.Add(Summary_text);
            document.Add(kwd);
            document.Add(jd_text);
            document.Add(km_text);
            document.Add(jobEDwords);
            document.Add(jobED);
            document.Add(resumeEDwords);
            document.Add(resumeED);
            document.Add(jobYOEwords);
            document.Add(resumeYOEwords);
            document.Add(matching);
            document.Add(matchingkw);
            document.Add(notmatching);
            document.Add(matchingnkw);
          
            //document.Add(resume_text);
           
            //document.Add(job_text);

            using (var htmlWorker = new iTextSharp.text.html.simpleparser.HTMLWorker(document))
            {

                //HTMLWorker doesn't read a string directly but instead needs a TextReader (which StringReader subclasses)
                resume.SpacingAfter = 5;
                document.Add(resume);
                using (var sr = new StringReader(analysis.ResumeRaw))
                {

                    //Parse the HTML
                    htmlWorker.Parse(sr);
                }
                job.SpacingAfter = 5;
                document.Add(job);
                using (var sr2 = new StringReader(analysis.JobRaw))
                {

                    //Parse the HTML
                    htmlWorker.Parse(sr2);
                }
            }
         

            document.Close();
            writer.Close();
           

            byte[] bytesInStream = fs.ToArray();

           
            Response.Buffer = false;
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            //Set the appropriate ContentType.
            Response.ContentType = "Application/pdf";
            //Write the file content directly to the HTTP content output stream.
            Response.BinaryWrite(bytesInStream);
            Response.Flush();
            Response.End();

            return new FileStreamResult(fs, "Application/pdf");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreatePDFResume(string createResume)
        {

            
            MemoryStream fs = new MemoryStream();
            Document document = new Document(PageSize.A4, 30, 30, 30, 30);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();
            using (var htmlWorker = new iTextSharp.text.html.simpleparser.HTMLWorker(document)) {

                //HTMLWorker doesn't read a string directly but instead needs a TextReader (which StringReader subclasses)
                using (var sr = new StringReader(createResume)) {

                    //Parse the HTML
                    htmlWorker.Parse(sr);
                }
            }
           

            document.Close();
            writer.Close();


            byte[] bytesInStream = fs.ToArray();


            Response.Buffer = false;
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            //Set the appropriate ContentType.
            Response.ContentType = "Application/pdf";
            //Write the file content directly to the HTTP content output stream.
            Response.BinaryWrite(bytesInStream);
            Response.Flush();
            Response.End();

            return new FileStreamResult(fs, "Application/pdf");
        }

        
    }
}