﻿using Bootstrap.Helpers;
using Bootstrap.Models;
using GemBox.Document;
using HtmlAgilityPack;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Bootstrap.Controllers
{
    public class RemoteController : Controller
    {
        // GET: API
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Compare(string resume, string job)
        {
            Analyze analysis = new Analyze();
            Results result = new Results();


            try
            {


                ApplicationDbContext db = new ApplicationDbContext();

                string cleanresume = HtmlSanitizer.Sanitize(resume);
                string cleanjob = HtmlSanitizer.Sanitize(job);


                SovrenHelper resumexml = new SovrenHelper();
                string rxml = resumexml.GetResumeXML(cleanresume);


                SovrenJob jobxml = new SovrenJob();
                string xml = jobxml.GetJobXml(cleanjob);

                Job outJob = new Job();
                Resume outResume = new Resume();

                outResume.Competencies = resumexml.GetCompetencies(rxml);
                outResume.Education = resumexml.GetEducation(rxml);
                outResume.Experience = resumexml.GetExperience(rxml);
                outResume.SingleValues = resumexml.GetResumeArray(rxml);
                outResume.Taxonomies = resumexml.GetBestFitTaxonomies(rxml);
                outResume.BestTax = outResume.Taxonomies.FirstOrDefault();
                outResume.ResumeText = resume;

                outJob = jobxml.GetJobArray(xml);
                outJob.JobText = job;
                string jt = outJob.SingleValues["JobTitle"];

                analysis = result.Compare(outJob, outResume);
                analysis.JobRaw = job;
                //clean resume good//

                HtmlUtility hcleaner = new HtmlUtility();
                analysis.ResumeRaw = resume;
                analysis.JobBestTax = outJob.BestTax;
                analysis.JobTitle = jt;

                analysis.JobXML = xml;
                analysis.ResumeXML = rxml;

                var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string sJSON = oSerializer.Serialize(analysis);
                
                return Json(new { StatusCode = 200, Message = "Successful Request", Result = sJSON });
            }
            catch (Exception ex)
            {
                //exception occured
               
                return Json(new { StatusCode = 400, Message = ex.Message, Result = "" });
            }




        }


        [HttpPost]
        public JsonResult ParseResume()
        {

            string finaltext = "";
            Analyze analysis = new Analyze();
            Results result = new Results();
            Resume outResume = new Resume();
            string identifier = "";
            try
            {

                
                //ApplicationDbContext db = new ApplicationDbContext();

                //string cleanresume = HtmlSanitizer.Sanitize(resume);

                if (Request.Files.AllKeys.Any())
                {
                    // Get the uploaded image from the Files collection
                    var httpPostedFile = Request.Files["UploadedFile"];
                    var zip = Request.Form["zip"];
                    identifier = Request.Form["identifier"];
                    
                    if (httpPostedFile != null)
                    {
                        ComponentInfo.SetLicense("DBTX-8EIK-50LW-QTKQ");

                        if (httpPostedFile.ContentType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                        {
                            DocumentModel document = DocumentModel.Load(httpPostedFile.InputStream, LoadOptions.DocxDefault);
                            StringBuilder sb = new StringBuilder();

                            foreach (GemBox.Document.Paragraph paragraph in document.GetChildElements(true, ElementType.Paragraph))
                            {
                                foreach (Run run in paragraph.GetChildElements(true, ElementType.Run))
                                {
                                    bool isBold = run.CharacterFormat.Bold;
                                    string text = run.Text;

                                    sb.AppendFormat("{0}{1}{2}", isBold ? "<b>" : "", text, isBold ? "</b>" : "");
                                }
                                sb.AppendLine();
                            }

                            finaltext = sb.ToString();
                            finaltext = finaltext.Replace("\n", "<br/>");
                        }

                        if (httpPostedFile.ContentType == "application/msword")
                        {

                            DocumentModel document = DocumentModel.Load(httpPostedFile.InputStream, LoadOptions.DocDefault);
                            StringBuilder sb = new StringBuilder();

                            foreach (GemBox.Document.Paragraph paragraph in document.GetChildElements(true, ElementType.Paragraph))
                            {
                                foreach (Run run in paragraph.GetChildElements(true, ElementType.Run))
                                {
                                    bool isBold = run.CharacterFormat.Bold;
                                    string text = run.Text;

                                    sb.AppendFormat("{0}{1}{2}", isBold ? "<b>" : "", text, isBold ? "</b>" : "");
                                }
                                sb.AppendLine();
                            }

                            finaltext = sb.ToString();
                            finaltext = finaltext.Replace("\n", "<br/>");
                        }

                        if (httpPostedFile.ContentType == "application/pdf")
                        {
                            string currentText = "";
                            PdfReader reader = new PdfReader(httpPostedFile.InputStream);

                            for (int page = 1; page <= reader.NumberOfPages; page++)
                            {
                                ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                                currentText += PdfTextExtractor.GetTextFromPage(reader, page, strategy);




                            }
                            currentText = Encoding.UTF8.GetString(ASCIIEncoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(currentText)));
                            finaltext = currentText.Replace("\n", "<br/>");

                        }

                        if (httpPostedFile.ContentType == "text/plain")
                        {
                            DocumentModel document = DocumentModel.Load(httpPostedFile.InputStream, LoadOptions.TxtDefault);

                            StringBuilder sb = new StringBuilder();

                            foreach (GemBox.Document.Paragraph paragraph in document.GetChildElements(true, ElementType.Paragraph))
                            {
                                foreach (Run run in paragraph.GetChildElements(true, ElementType.Run))
                                {
                                    bool isBold = run.CharacterFormat.Bold;
                                    string text = run.Text;

                                    sb.AppendFormat("{0}{1}{2}", isBold ? "<b>" : "", text, isBold ? "</b>" : "");
                                }
                                sb.AppendLine();
                            }
                            finaltext = sb.ToString();
                        }
                    }
                }


                SovrenHelper resumexml = new SovrenHelper();
                string rxml = resumexml.GetResumeXML(finaltext);


                outResume.Competencies = resumexml.GetCompetencies(rxml);
                outResume.Education = resumexml.GetEducation(rxml);
                outResume.Experience = resumexml.GetExperience(rxml);
                outResume.SingleValues = resumexml.GetResumeArray(rxml);
                outResume.Taxonomies = resumexml.GetBestFitTaxonomies(rxml);
                outResume.BestTax = outResume.Taxonomies.FirstOrDefault();
                outResume.ResumeText = finaltext;

                var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string sJSON = oSerializer.Serialize(outResume);
               
                return Json(new { StatusCode = 200, Message = "Successful Request", Result = sJSON, identifier = identifier });
            }
            catch (Exception ex)
            {
                //exception occured
                
                return Json(new { StatusCode = 400, Message = ex.Message, Result = "" });
            }




        }


        [HttpPost]
        public JsonResult ParseJob(string job, string identifier)
        {
            Analyze analysis = new Analyze();
            Results result = new Results();

            try
            {


                ApplicationDbContext db = new ApplicationDbContext();

                string cleanjob = HtmlSanitizer.Sanitize(job);

                SovrenJob jobxml = new SovrenJob();
                string xml = jobxml.GetJobXml(cleanjob);

                Job outJob = new Job();

                outJob = jobxml.GetJobArray(xml);
                outJob.JobText = job;

                var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string sJSON = oSerializer.Serialize(outJob);
               
                return Json(new { StatusCode = 200, Message = "Successful Request", Result = sJSON, identifier = identifier});
            }
            catch (Exception ex)
            {
                //exception occured
               
                return Json(new { StatusCode = 400, Message = ex.Message, Result = "" });
            }




        }


        [HttpGet]
        public JsonResult SearchIndeed(string search, string loc)
        {
            HttpWebRequest web_request = (HttpWebRequest)WebRequest.Create("http://api.indeed.com/ads/apisearch?publisher=5422421003768593&q=" + search + "&l=" + loc + "&sort=&radius=&st=&jt=&format=json&start=&limit=&fromage=&filter=&latlong=1&co=us&chnl=&userip=&useragent=&v=2");
            web_request.Method = "GET";
            web_request.ContentType = "application/json; charset=utf-8";
            var response_string = "";
            using (HttpWebResponse web_response = (HttpWebResponse)web_request.GetResponse())
            {
                using (StreamReader response_stream = new StreamReader(web_response.GetResponseStream()))
                {
                    response_string = response_stream.ReadToEnd();
                }
            }
            
            return Json(new { response_string },JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetJobFromIndeed(string url, string identifier)
        {


            HttpWebRequest web_request = (HttpWebRequest)WebRequest.Create(url);
            var response_string = "";
            using (HttpWebResponse web_response = (HttpWebResponse)web_request.GetResponse())
            {
                using (StreamReader response_stream = new StreamReader(web_response.GetResponseStream()))
                {
                    response_string = response_stream.ReadToEnd();
                }
            }

            HtmlDocument doc = new HtmlDocument();

            doc.LoadHtml(response_string);

            var jobdesc = doc.GetElementbyId("job_summary").InnerHtml;
            //split by Compensation:???//
            var jobtitle = doc.GetElementbyId("job_header").InnerHtml;
           
            
            return Json(new {StatusCode = 200, Message="Successful Request", JobDesc = jobdesc, JobTitle = jobtitle, identifier = identifier });

        }

    }





}




