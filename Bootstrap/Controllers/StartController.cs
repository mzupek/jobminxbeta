﻿using Bootstrap.Helpers;
using Bootstrap.Models;
using HtmlAgilityPack;
using MailChimp;
using MailChimp.Helper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Bootstrap.Controllers
{
    public class StartController : Controller
    {
        // GET: Start

         public StartController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        public StartController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }

        public ActionResult Index()
        {

            
            return View();
        }

        public ActionResult FreeTrial()
        {
            
            

            return View();
        }
        [HttpPost]
        public ActionResult Test()
        {

            FreeTrials ft = new FreeTrials();
            int ftl = ft.TriesLeft(Request.UserHostAddress);

            if (ftl == 0)
            {
                return Json(new { result = 0 });
            }
            else
            {
                return Json(new { result = 1 });

            }

        }

        public ActionResult FreeTrialLander ()
        {

            return View();
        }
        
        [HttpPost]
        [ValidateInput(false)] 
        public ActionResult FreeTrial(string resume, string job, bool sample)
        {
            if(sample == true)
            {
                return RedirectToAction("FreeTrialLander");
            }
            else
            {

                FreeTrials ft = new FreeTrials();
                int ftl = ft.TriesLeft(Request.UserHostAddress);

                if (ftl == 0)
                {
                    return RedirectToAction("Redirect");
                }
                else
                {



                    Cleaner clean = new Cleaner();
                    string cleanJob = clean.CleanInput(job);
                    string cleanResume = clean.CleanInput(resume);

                    SovrenHelper resumexml = new SovrenHelper();
                    string rxml = resumexml.GetResumeXML(cleanResume);


                    SovrenJob jobxml = new SovrenJob();
                    string xml = jobxml.GetJobXml(cleanJob);
                    Job outJob = new Job();
                    Resume outResume = new Resume();

                    outResume.Competencies = resumexml.GetCompetencies(rxml);
                    outResume.Education = resumexml.GetEducation(rxml);
                    outResume.Experience = resumexml.GetExperience(rxml);
                    outResume.SingleValues = resumexml.GetResumeArray(rxml);


                    outJob = jobxml.GetJobArray(xml);


                    Analyze analysis = new Analyze();
                    Results result = new Results();

                    analysis = result.Compare(outJob, outResume);
                    analysis.JobRaw = job;
                    analysis.ResumeRaw = resume;
                    int ul = ft.IncrementTries(Request.UserHostAddress);
                    TempData["analysis"] = analysis;
                    return RedirectToAction("Results");
                }
            }
        }

        public ActionResult Results()
        {
            Analyze analysis = new Analyze();
            analysis = TempData["analysis"] as Analyze;
            return View(analysis);
        }

        
        public ActionResult Redirect()
        {

            return View();
        }

        public ActionResult Pricing()
        {

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SignUp(signup model)
        {
            if (ModelState.IsValid)
            {
                Merchant trans = new Merchant();

                PurchaseSummary transaction = new PurchaseSummary();
                

                IdentityResult userresult = new IdentityResult();
                ApplicationUser user = new ApplicationUser();

                //MerchantResponse result = new MerchantResponse();
                //result.IsTransactionApproved = true;


                string[] plans = { "NA", "Trial", "Monthly", "Extended" };
                ApplicationDbContext db = new ApplicationDbContext();
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {

                    try
                    {

                        if (model.offers != true)
                        {
                            model.offers = false;
                        }

                        //set up users profile

                        ApplicationDbContext.UserProfile up = new ApplicationDbContext.UserProfile();
                        up.EmailAddress = model.email;
                        up.UserName = model.email;
                        up.Plan1 = model.plan.ToString();
                        up.IpAddress = HttpContext.Request.UserHostAddress;
                        up.CreatedDate = DateTime.Now;
                        up.Offers = model.offers;
                        if (model.plan == 3)
                        {
                            up.PlanExperation = DateTime.Now.AddMonths(3);
                        }
                        else
                        {
                            up.PlanExperation = DateTime.Now.AddMonths(1);
                        }
                        up.Terms = model.terms;

                        int toks = 5;
                        
                        up.Tokens = toks;
                        //save entity
                        db.UserProfiles.Add(up);

                        // make it ok to use email UI//
                        var userValidator = UserManager.UserValidator as UserValidator<ApplicationUser>;
                        userValidator.AllowOnlyAlphanumericUserNames = false;


                        //create asp.net user
                        user = new ApplicationUser() { UserName = model.email };
                        userresult = await UserManager.CreateAsync(user, model.password);

                        if (userresult.Succeeded)
                        {
                           
                                ApplicationDbContext.ActionItem a = new ApplicationDbContext.ActionItem();

                                a.PromoId = model.plan;
                                a.UserName = up.UserName;
                                a.UserId = 0;
                                a.PurchaseDate = DateTime.Now;
                                a.Expiration = DateTime.Now.AddMonths(1);
                                db.ActionItems.Add(a);
                                transaction.username = user.UserName;
                                transaction.email = user.UserName;
                                transaction.orderNumber = a.TransactionId;
                                transaction.plan = plans[Convert.ToInt32(model.plan)];
                                transaction.password = model.password;
                                transaction.offers = model.offers;

                                transaction.tokens = up.Tokens;
                                transaction.total = "$0.00";
                                transaction.tokenSummary = "Tokens Purchased: " + up.Tokens;
                                TempData["transaction"] = transaction;
                                db.SaveChanges();
                                dbContextTransaction.Commit();

                                await SignInAsync(user, isPersistent: false);
                                //send welcome email
                                Messenger emailit = new Messenger();
                                string smtpresponse = emailit.SendWelcomeMessageFree(transaction);
                                //MailChimpManager mc = new MailChimpManager("267d9c698c4a9baa3ddc607dd7a1f01c-us7");

                                ////  Create the email parameter
                                //EmailParameter email = new EmailParameter()
                                //{
                                //    Email = model.email
                                    
                                //};

                                //EmailParameter results = mc.Subscribe("f9d9c2c74a", email, null, "html",false,false,false,false);
                                return RedirectToAction("OrderCompleteFree");
                          

                            }
                        }

                    
                    catch (Exception ex)
                    {
                        ModelState.AddModelError("email", ex);
                        return View(model);
                    }



                }



            }
            ModelState.AddModelError("email", "An Error has occured with your form, please review and try again.");
            return View(model);
                
        }
            
        public ActionResult Purchase()
        {
            var plan = Request["plan"];
            var upgrade = Request["upgrade"];
            ViewBag.plan = plan;
            ViewBag.upgrade = upgrade;
            return View();
        }

        public ActionResult SignUp()
        {
            
            return View();
        }

        public ActionResult PurchaseUpgrade()
        {

            var plan = Request["plan"];
            var upgrade = Request["upgrade"];
            ViewBag.plan = plan;
            ViewBag.upgrade = upgrade;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult PurchaseUpgrade(purchaseupgrade model)
        {

            if (ModelState.IsValid)
            {
                Merchant trans = new Merchant();

                PurchaseSummary transaction = new PurchaseSummary();
                string exdate = model.expiryMonth + model.expiryYear;

               

                //MerchantResponse result = new MerchantResponse();
                //result.IsTransactionApproved = true;


                string[] plans = { "NA", "Trial", "Monthly", "Extended" };
                ApplicationDbContext db = new ApplicationDbContext();
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {

                    try
                    {

                        if (model.offers != true)
                        {
                            model.offers = false;
                        }



                        //find current user
                        var up = db.UserProfiles.Where(c => c.UserName == User.Identity.Name).SingleOrDefault();
                        //userresult = await UserManager.CreateAsync(user, model.password);

                        if (up != null)
                        {


                            MerchantResponse result = trans.charge(model.cardHolderName, model.cardNumber, exdate, model.total);
                            //if cc is approved

                            if (result.IsTransactionApproved)
                            {
                                //create trans record
                                ApplicationDbContext.ActionItem a = new ApplicationDbContext.ActionItem();

                                a.Expiration = Convert.ToDateTime(model.expiryMonth + "/" + model.expiryYear);
                                a.Worth = result.ammount;
                                a.PromoId = model.plan;
                                a.UserName = up.UserName;
                                a.UserId = 0;
                                a.PurchaseDate = DateTime.Now;
                                a.Expiration = DateTime.Now.AddMonths(1);
                                a.TransactionId = result.authNum;
                                a.ctr = result.ctr;
                                db.ActionItems.Add(a);
                                //set up trans result
                                transaction.username = up.UserName;
                                transaction.email = up.UserName;
                                transaction.orderNumber = a.TransactionId;
                                transaction.plan = plans[Convert.ToInt32(model.plan)];
                                transaction.password = "";
                                transaction.offers = model.offers;
                                transaction.ctr = result.ctr;
                                transaction.exDate = result.exDate;
                                transaction.tokens = up.Tokens;
                                transaction.total = result.ammount;
                                transaction.tokenSummary = "Plan Summary: " + plans[Convert.ToInt32(model.plan)];
                                TempData["transaction"] = transaction;
                                
                                up.Plan1 = model.plan.ToString();
                                if(model.plan == 2)
                                {
                                    up.PlanExperation = DateTime.Now.AddMonths(1);
                                }
                                else
                                {
                                    up.PlanExperation = DateTime.Now.AddMonths(3);
                                }
                                up.Tokens = 0;
                                up.Offers = model.offers;
                                up.Terms = model.terms;

                                //save and commit records
                                db.SaveChanges();
                                dbContextTransaction.Commit();

                                

                               
                                //send welcome email
                                Messenger emailit = new Messenger();
                                string smtpresponse = emailit.SendWelcomeMessage(transaction, model.cardHolderName);
                               
                                return RedirectToAction("OrderComplete");
                            }
                            else
                            {
                                dbContextTransaction.Rollback();
                                ModelState.AddModelError("email", new System.ArgumentException("There was an issue", "original"));
                                return View(model);
                            }


                        }

                    }
                    catch (Exception ex)
                    {
                        ModelState.AddModelError("email", ex);
                        return View(model);
                    }



                }



            }
            ModelState.AddModelError("email", "An Error has occured with your form, please review and try again.");
            return View(model);
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Purchase(purchase model)
        {
            
            if (ModelState.IsValid)
            {
                Merchant trans = new Merchant();

                PurchaseSummary transaction = new PurchaseSummary();
                string exdate = model.expiryMonth + model.expiryYear;

                IdentityResult userresult = new IdentityResult();
                ApplicationUser user = new ApplicationUser();

                //MerchantResponse result = new MerchantResponse();
                //result.IsTransactionApproved = true;


                string[] plans = { "NA", "Trial", "Monthly", "Extended" };
                ApplicationDbContext db = new ApplicationDbContext();
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {

                    try
                    {

                        if (model.offers != true)
                        {
                            model.offers = false;
                        }

                        //set up users profile

                        ApplicationDbContext.UserProfile up = new ApplicationDbContext.UserProfile();
                        up.EmailAddress = model.email;
                        up.UserName = model.email;
                        up.Plan1 = model.plan.ToString();
                        up.IpAddress = HttpContext.Request.UserHostAddress;
                        up.CreatedDate = DateTime.Now;
                        up.Offers = model.offers;
                        if(model.plan == 3)
                        {
                            up.PlanExperation = DateTime.Now.AddMonths(3);
                        }
                        else
                        {
                            up.PlanExperation = DateTime.Now.AddMonths(1);
                        }
                        
                        up.Terms = model.terms;

                        int toks = 0;
                        if (up.Plan1 == "1")
                        {
                            toks = 5;
                        }
                        if (up.Plan1 == "2")
                        {
                            toks = 0;
                        }
                        if (up.Plan1 == "3")
                        {
                            toks = 0;
                        }
                        up.Tokens = toks;
                        //save entity
                        db.UserProfiles.Add(up);

                        // make it ok to use email UI//
                        var userValidator = UserManager.UserValidator as UserValidator<ApplicationUser>;
                        userValidator.AllowOnlyAlphanumericUserNames = false;
                        
                        
                        //create asp.net user
                        user = new ApplicationUser() { UserName = model.email };
                        userresult = await UserManager.CreateAsync(user, model.password);

                        if (userresult.Succeeded)
                        {
                           
 
                                MerchantResponse result = trans.charge(model.cardHolderName, model.cardNumber, exdate, model.total);
                                //if cc is approved

                                if (result.IsTransactionApproved)
                                {
                                    //create trans record
                                    ApplicationDbContext.ActionItem a = new ApplicationDbContext.ActionItem();

                                    a.Expiration = Convert.ToDateTime(model.expiryMonth + "/" + model.expiryYear);
                                    a.Worth = result.ammount;
                                    a.PromoId = model.plan;
                                    a.UserName = up.UserName;
                                    a.UserId = 0;
                                    a.PurchaseDate = DateTime.Now;
                                    a.Expiration = up.PlanExperation;
                                    a.TransactionId = result.authNum;
                                    a.ctr = result.ctr;
                                    db.ActionItems.Add(a);
                                    //set up trans result
                                    transaction.username = user.UserName;
                                    transaction.email = user.UserName;
                                    transaction.orderNumber = a.TransactionId;
                                    transaction.plan = plans[Convert.ToInt32(model.plan)];
                                    transaction.password = model.password;
                                    transaction.offers = model.offers;
                                    transaction.ctr = result.ctr;
                                    transaction.exDate = result.exDate;
                                    transaction.tokens = up.Tokens;
                                    transaction.total = result.ammount;
                                    transaction.tokenSummary = "Plan Summary: " + plans[Convert.ToInt32(model.plan)];
                                    TempData["transaction"] = transaction;

                                    //save and commit records
                                    db.SaveChanges();
                                    dbContextTransaction.Commit();

                                    //define user & sign user in

                                    await SignInAsync(user, isPersistent: false);
                                    //send welcome email
                                    Messenger emailit = new Messenger();
                                    string smtpresponse = emailit.SendWelcomeMessage(transaction, model.cardHolderName);
                                    return RedirectToAction("OrderComplete");
                                }
                                else
                                {
                                    dbContextTransaction.Rollback();
                                    ModelState.AddModelError("email", new System.ArgumentException("There was an issue, please try again", "original"));
                                    return View(model);
                                }

                            
                        }

                    }
                    catch (Exception ex)
                    {
                        ModelState.AddModelError("email", "An Error has occured with your form, please review and try again.");
                        return View(model);
                    }
                    
                    

                }

                

            }
            ModelState.AddModelError("email", "This email is already registered.");
            return View(model);
        }

        public ActionResult OrderComplete()
        {
            PurchaseSummary transaction = new PurchaseSummary();
            transaction = TempData["transaction"] as PurchaseSummary;

            return View(transaction);
        }

        public ActionResult OrderCompleteFree()
        {
            PurchaseSummary transaction = new PurchaseSummary();
            transaction = TempData["transaction"] as PurchaseSummary;

            return View(transaction);
        }

        //Identity Set up

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }


        public ActionResult Contact()
        {

            return View();

        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(string toAddress, string firstname, string message)
        {

            Messenger contact = new Messenger();
            string result = contact.SendContactConfirmation(toAddress);
            string result2 = contact.SendInfoMessage(toAddress, firstname, message);
            if(result !="200" || result2 != "200")
            {

                ViewBag.outcome = "There was an error in proccessing your request, please try again later.";
            }
            else
            {

                ViewBag.outcome = "Thanks for contacting us, we will be talking to you soon!";
            }

            return View();

        }

        public ActionResult About()
        {




            return View();

        }

        public ActionResult FAQ()
        {




            return View();
        }


        public ActionResult TermsConditions()
        {




            return View();
        }

        public ActionResult PrivacyPolicy()
        {




            return View();
        }
        
        public ActionResult Connect()
        {


            return View();
        }

        public ActionResult Connection()
        {
            string url = Request["url"];
            string jobId = Request["jobId"];

            HttpWebRequest web_request = (HttpWebRequest)WebRequest.Create(url);
            var response_string = "";
            using (HttpWebResponse web_response = (HttpWebResponse)web_request.GetResponse())
            {
                using (StreamReader response_stream = new StreamReader(web_response.GetResponseStream()))
                {
                    response_string = response_stream.ReadToEnd();
                }
            }

            HtmlDocument doc = new HtmlDocument();

            doc.LoadHtml(response_string);

            var jobdesc = doc.GetElementbyId(jobId).InnerHtml;

            ViewBag.job = jobdesc;
            

            return View();
        }



    }
}