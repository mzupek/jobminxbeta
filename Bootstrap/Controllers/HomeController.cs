﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bootstrap.Models;
using Bootstrap.Helpers;
using Bootstrap.Api.DTO;

namespace Bootstrap.Controllers
{
    public class HomeController : Controller
    {
        JMContext db = new JMContext();
        [HttpGet]
        public ActionResult Index()
        {

            FreeTrials connect = new FreeTrials();
            
            var ip = Request.UserHostAddress.ToString();

            int usesleft = connect.TriesLeft(ip);
            ViewBag.left = usesleft;
            //add logic for hard redirect.. using viewbag becuase its simple for client
            return View();
        }
        
        [HttpPost]
        public ActionResult Index(Compare model)
        {

            //get ip
            var ip = Request.UserHostAddress.ToString();
            //total allowed
            FreeTrials connect = new FreeTrials();
            int usesleft = connect.TriesLeft(ip);
            
            if (usesleft > 0)
            {

                Attempt att = new Attempt();
                att.IpAddress = ip;
                db.Attempts.Add(att);
                db.SaveChanges();
                var n = db.Attempts.Where(m => m.IpAddress == ip).ToList();
                int userAttn = n.Count();
                int usesleftn = usesleft - userAttn;
                ViewBag.left = usesleftn;


            }
            else
            {
                ViewBag.left = 0;
            }
            
            //init sovren resume api
            SovrenHelper resumeRef = new SovrenHelper();
            //Parse the resume to XML//
            string resume = resumeRef.GetResumeXML(model.resume);
            Dictionary<string,string> output = resumeRef.GetResumeArray(resume);
            List<string> comps = resumeRef.GetCompetencies(resume);
            List<Dictionary<string, string>> ex = resumeRef.GetExperience(resume);
            //done with resume//
            
            //init sovren job api --- coming soon ---//
            JobParse job = new JobParse();
            DocumentResult result = job.GetResults(model.job);
            //done with job//

            //what should you be... we need to figure out what job keyword we should search for on Onet//
            //init Onet//
           // Onet jobs = new Onet();
            //get the code back//
          //  string code = jobs.GetJobCodes("web administrator");
            //all of the other calls in 1 - dont forget about the Map of Job Saturation//
         //   string careerOverview = jobs.GetCareerOverview(code);
            //--------------singles---------------//
         //   string careerReport = jobs.GetCareerReport(code);
         //   string education = jobs.GetEducation(code);
         //   string abilities = jobs.GetAbilities(code);
         //   string knowledge = jobs.GetKnowledge(code);
         //   string personality = jobs.GetPersonality(code);
         //   string skills = jobs.GetSkills(code);
         //   string technology = jobs.GetTechnology(code);
            //done with O'Net//



            //build ideal canidate//


            model.ResumeCompetencies = comps;
            model.ResumeOutput = output;

            return View("Analysis", model);
        }
        
        public ActionResult Analysis(Compare model)
        {

            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Pricing()
        {


            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}