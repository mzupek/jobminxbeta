﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bootstrap.com.resumeparsing.services1;
using Bootstrap.Helpers;
using Bootstrap.Models;
using System.Web.Script.Serialization;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;

namespace Bootstrap.Controllers
{
    public class OptimizeController : Controller
    {
        public OptimizeController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        public OptimizeController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }
        //
        // GET: /Optimize/
        public ActionResult Index()
        {
            FreeTrials ft = new FreeTrials();
            var ip = Request.UserHostAddress.ToString();
            int count = ft.TriesLeft(ip);
            ViewBag.tries = count;
            return View();
        }

        public ActionResult TestFreeTries(string ip)
        {
            FreeTrials ft = new FreeTrials();
            int count = ft.TriesLeft(ip);
            if (count <= 0)
            {
                count = 0;
            }
            return Json(new { tries = count });

        }

        public ActionResult AddTry(string ip)
        {
            FreeTrials ft = new FreeTrials();
            int count = ft.TriesLeft(ip);
            if (count <= 0)
            {
                count = 0;
            }
            return Json(new { tries = count });

        }

        public ActionResult Auth()
        {

            return View();
        }

    }
}