﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bootstrap.Models;
using System.Data.Entity.Core.Objects;

namespace Bootstrap.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {

            ApplicationDbContext db = new ApplicationDbContext();
            var users = db.UserProfiles.ToList();
            ViewBag.UserCount = users.Count();
            var s = db.ActionItems.ToList();
            var s1 = s.Sum(r => Convert.ToDecimal(r.Worth));
            ViewBag.Total = s1;
            var ot = db.ActionItems.Where(c => EntityFunctions.TruncateTime(c.PurchaseDate) == DateTime.Today).ToList();
            ViewBag.Orders = ot.Count();
            var TodayTotal = ot.Sum(r => Convert.ToDecimal(r.Worth));
            ViewBag.Today = TodayTotal;
            return View();
        }

        public ActionResult Api()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var q = db.apis.Find(1);
            ViewBag.calls = q.Worth;
            return View();

        }

        public ActionResult Users()
        {

            ApplicationDbContext db = new ApplicationDbContext();
            var q = db.UserProfiles.ToList();
            return View(q);


        }

        public ActionResult Transactions()
        {

            ApplicationDbContext db = new ApplicationDbContext();
            var q = db.ActionItems.ToList();
            return View(q);


        }

        public ActionResult Demo()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var q = db.UserResumes.ToList();
            return View(q);
        }

        public ActionResult Support()
        {

            return View();
        }


        [Authorize]
        [HttpPost]   
        public ActionResult GetActions()
        {

            JMContext db = new JMContext();
            var q = db.ActionItems.ToList();
            var s = q.OrderByDescending(w => w.PurchaseDate).Take(5);
            return PartialView("_GetActions", s);

        }

        [Authorize]
        [HttpPost]
        public ActionResult GetSales()
        {

            JMContext db = new JMContext();
            var plan1 = db.ActionItems.Where(p=>p.PromoId == 1).ToList();
            var plan2 = db.ActionItems.Where(p => p.PromoId == 2).ToList();
            var plan3 = db.ActionItems.Where(p => p.PromoId == 3).ToList();
            var plan4 = db.ActionItems.Where(p => p.PromoId == 4).ToList();
            var s1 = plan1.Sum(r => Convert.ToDecimal(r.Worth));
            var s2 = plan2.Sum(r => Convert.ToDecimal(r.Worth));
            var s3 = plan3.Sum(r => Convert.ToDecimal(r.Worth));
            var s4 = plan4.Sum(r => Convert.ToDecimal(r.Worth));
            return Json(new{plan1 = s1, plan2 = s2, plan3 = s3, plan4 = s4});

        }


        [Authorize]
        [HttpPost]
        public ActionResult GetLastestUser()
        {

            JMContext db = new JMContext();
            var q = db.UserProfiles.ToList();
            var s = q.OrderByDescending(w => w.CreatedDate).Take(5);
            return PartialView("_GetLastestUser", s);

        }
        [Authorize]
        
        public ActionResult GetUsage()
        {
            //JMContext db = new JMContext();
            
            //var  query = from r in db.Engagements.ToList()
            //            group r by r.UserId into g
            //            select new { Count = g.Count(), Value = g.Key };
            
            return View();


        }
    }
}