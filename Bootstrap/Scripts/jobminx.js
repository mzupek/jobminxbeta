﻿(function ($) {

    $.jobminx = function( jobId, jobButton, Url) {
 
        var imageUrl = "https://jobminx.com/Content/connect.png";
        $("#" + jobButton).css('background-image', 'url(' + imageUrl + ')');
        $("#" + jobButton).css('cursor', 'pointer');
        $("#" + jobButton).css('width', '250px');
        $("#" + jobButton).css('height', '96px');
        $("#" + jobButton).css('padding', '5px');
        $("#" + jobButton).css('background-size', '250px 96px');
 
        $("#"+jobButton).click(function () {

            
            window.open("https://jobminx.com/Start/Connection?id=0&url=" + Url + "&jobId=" + jobId, "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=500, left=500, width=400, height=400");

        });

        

        return this;

    };

}(jQuery));

