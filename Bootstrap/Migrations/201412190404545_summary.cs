namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class summary : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserResume", "Experience", c => c.String());
            AddColumn("dbo.UserResume", "Education", c => c.String());
            AddColumn("dbo.UserResume", "Skills", c => c.String());
            AddColumn("dbo.UserResume", "Summary", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserResume", "Summary");
            DropColumn("dbo.UserResume", "Skills");
            DropColumn("dbo.UserResume", "Education");
            DropColumn("dbo.UserResume", "Experience");
        }
    }
}
