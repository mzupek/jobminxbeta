namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class terms : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfile", "Terms", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfile", "Terms");
        }
    }
}
