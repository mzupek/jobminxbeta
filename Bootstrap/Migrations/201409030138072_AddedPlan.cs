namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPlan : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfile", "Handle", c => c.String());
            AddColumn("dbo.UserProfile", "Plan", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfile", "Plan");
            DropColumn("dbo.UserProfile", "Handle");
        }
    }
}
