namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class offers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfile", "Offers", c => c.Boolean(nullable: false));
            DropColumn("dbo.UserProfile", "Status");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfile", "Status", c => c.Int(nullable: false));
            DropColumn("dbo.UserProfile", "Offers");
        }
    }
}
