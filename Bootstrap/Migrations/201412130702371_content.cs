namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class content : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserResume", "ContentType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserResume", "ContentType");
        }
    }
}
