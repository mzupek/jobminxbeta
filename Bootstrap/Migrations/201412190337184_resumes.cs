namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class resumes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserResume", "ZipCode", c => c.String());
            AddColumn("dbo.UserResume", "Category", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserResume", "Category");
            DropColumn("dbo.UserResume", "ZipCode");
        }
    }
}
