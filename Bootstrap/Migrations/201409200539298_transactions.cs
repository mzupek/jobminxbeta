namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class transactions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Payments_Id", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "Payments_Id");
            AddForeignKey("dbo.AspNetUsers", "Payments_Id", "dbo.ActionItem", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "Payments_Id", "dbo.ActionItem");
            DropIndex("dbo.AspNetUsers", new[] { "Payments_Id" });
            DropColumn("dbo.AspNetUsers", "Payments_Id");
        }
    }
}
