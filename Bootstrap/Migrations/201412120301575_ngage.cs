namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ngage : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Engagement", "UserId", c => c.String());
            AlterColumn("dbo.Engagement", "Plan", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Engagement", "Plan", c => c.Int(nullable: false));
            AlterColumn("dbo.Engagement", "UserId", c => c.Int(nullable: false));
        }
    }
}
