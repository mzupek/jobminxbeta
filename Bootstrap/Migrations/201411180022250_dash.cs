namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dash : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.api",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Worth = c.Int(nullable: false),
                        Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Engagement",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        Plan = c.Int(nullable: false),
                        Expiration = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.Promo");
            DropTable("dbo.Subscription");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Subscription",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        Expiration = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Promo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Worth = c.Int(nullable: false),
                        Expiration = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.Engagement");
            DropTable("dbo.api");
        }
    }
}
