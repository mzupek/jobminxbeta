namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ip : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfile", "IpAddress", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfile", "IpAddress");
        }
    }
}
