namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class resume : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserResume", "Name", c => c.String());
            AddColumn("dbo.UserResume", "DateModified", c => c.DateTime(nullable: false));
            AddColumn("dbo.UserResume", "DateLoaded", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserResume", "DateLoaded");
            DropColumn("dbo.UserResume", "DateModified");
            DropColumn("dbo.UserResume", "Name");
        }
    }
}
