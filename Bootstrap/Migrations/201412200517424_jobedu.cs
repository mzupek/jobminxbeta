namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class jobedu : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfile", "JobAlert", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfile", "JobAlert");
        }
    }
}
