namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addpDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ActionItem", "UserName", c => c.String());
            AddColumn("dbo.ActionItem", "PurchaseDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ActionItem", "PurchaseDate");
            DropColumn("dbo.ActionItem", "UserName");
        }
    }
}
