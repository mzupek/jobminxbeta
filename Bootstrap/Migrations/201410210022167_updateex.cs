namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateex : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfile", "PlanExperation", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfile", "PlanExperation");
        }
    }
}
