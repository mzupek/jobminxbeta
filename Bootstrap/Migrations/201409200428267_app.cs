namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class app : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "UserInfo_UserId", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "UserInfo_UserId");
            AddForeignKey("dbo.AspNetUsers", "UserInfo_UserId", "dbo.UserProfile", "UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "UserInfo_UserId", "dbo.UserProfile");
            DropIndex("dbo.AspNetUsers", new[] { "UserInfo_UserId" });
            DropColumn("dbo.AspNetUsers", "UserInfo_UserId");
        }
    }
}
