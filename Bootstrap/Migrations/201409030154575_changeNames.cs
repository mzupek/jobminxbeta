namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeNames : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActionItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        PromoId = c.Int(nullable: false),
                        TransactionId = c.Int(nullable: false),
                        CardType = c.String(),
                        Type = c.String(),
                        Worth = c.Int(nullable: false),
                        Expiration = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.Action");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Action",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        PromoId = c.Int(nullable: false),
                        TransactionId = c.Int(nullable: false),
                        CardType = c.String(),
                        Type = c.String(),
                        Worth = c.Int(nullable: false),
                        Expiration = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.ActionItem");
        }
    }
}
