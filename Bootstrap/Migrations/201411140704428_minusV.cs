namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class minusV : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUsers", "Payments_Id", "dbo.ActionItem");
            DropForeignKey("dbo.AspNetUsers", "UserInfo_UserId", "dbo.UserProfile");
            DropIndex("dbo.AspNetUsers", new[] { "Payments_Id" });
            DropIndex("dbo.AspNetUsers", new[] { "UserInfo_UserId" });
            DropColumn("dbo.AspNetUsers", "Payments_Id");
            DropColumn("dbo.AspNetUsers", "UserInfo_UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "UserInfo_UserId", c => c.Int());
            AddColumn("dbo.AspNetUsers", "Payments_Id", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "UserInfo_UserId");
            CreateIndex("dbo.AspNetUsers", "Payments_Id");
            AddForeignKey("dbo.AspNetUsers", "UserInfo_UserId", "dbo.UserProfile", "UserId");
            AddForeignKey("dbo.AspNetUsers", "Payments_Id", "dbo.ActionItem", "Id");
        }
    }
}
