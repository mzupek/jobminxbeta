namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newfields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfile", "Tokens", c => c.Int(nullable: false));
            AddColumn("dbo.UserProfile", "Status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfile", "Status");
            DropColumn("dbo.UserProfile", "Tokens");
        }
    }
}
