namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newplan : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfile", "Plan1", c => c.String());
            DropColumn("dbo.UserProfile", "Plan");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfile", "Plan", c => c.String());
            DropColumn("dbo.UserProfile", "Plan1");
        }
    }
}
