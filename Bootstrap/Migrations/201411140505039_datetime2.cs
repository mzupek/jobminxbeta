namespace Bootstrap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class datetime2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ActionItem", "PurchaseDate", c => c.DateTime());
            AlterColumn("dbo.ActionItem", "Expiration", c => c.DateTime());
            AlterColumn("dbo.Promo", "Expiration", c => c.DateTime());
            AlterColumn("dbo.Subscription", "Expiration", c => c.DateTime());
            AlterColumn("dbo.UserProfile", "CreatedDate", c => c.DateTime());
            AlterColumn("dbo.UserProfile", "PlanExperation", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserProfile", "PlanExperation", c => c.DateTime(nullable: false));
            AlterColumn("dbo.UserProfile", "CreatedDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Subscription", "Expiration", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Promo", "Expiration", c => c.DateTime(nullable: false));
            AlterColumn("dbo.ActionItem", "Expiration", c => c.DateTime(nullable: false));
            AlterColumn("dbo.ActionItem", "PurchaseDate", c => c.DateTime(nullable: false));
        }
    }
}
