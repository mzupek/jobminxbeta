﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml;
using System.Xml.XPath;

namespace Bootstrap.Helpers
{
    public class SimplyHiredJob
    {

        public string jobtitle { get; set; }
        public string jobdesc { get; set; }
        public string url { get; set; }
        public string anchor { get; set; }
        public string loc { get; set; }
        public string dp { get; set; }
    }


    public class SimplyHired
    {

        //api http://api.simplyhired.com/a/jobs-api/xml-v2/q-web?pshid=49596&ssty=2&cflg=r&jbd=jobminx.jobamatic.com&clip=

        public List<SimplyHiredJob> getJobs(string search, string ip)
        {
            List<SimplyHiredJob> jobslist = new List<SimplyHiredJob>();
            var urlsearch = HttpUtility.UrlEncode(search);
            string url = "http://api.simplyhired.com/a/jobs-api/xml-v2/q-" + urlsearch + "?pshid=49596&ssty=2&cflg=r&jbd=jobminx.jobamatic.com&clip=" + ip;
            HttpWebRequest web_request = (HttpWebRequest)WebRequest.Create(url);
            var response_string = "";
            using (HttpWebResponse web_response = (HttpWebResponse)web_request.GetResponse())
            {
                using (StreamReader response_stream = new StreamReader(web_response.GetResponseStream()))
                {
                    response_string = response_stream.ReadToEnd();
                }

                //load xml
                
                XPathDocument doc = new XPathDocument(new StringReader(response_string));
                XPathNavigator nav = doc.CreateNavigator();
               

                try
                {

                    XPathNavigator navDetail = nav.SelectSingleNode("(//rs)");
                    XPathNodeIterator it = navDetail.Select("//*");
                    int count = it.Current.SelectChildren(XPathNodeType.Element).Count;
                    for (int j = 1; j <= count; j++)
                    {
                        SimplyHiredJob myjob = new SimplyHiredJob();
                       
                        navDetail = nav.SelectSingleNode("(//r/src/@url)[" + j + "]");
                        myjob.url = navDetail.Value;
                        navDetail = nav.SelectSingleNode("(//r/cn)[" + j + "]");
                        myjob.anchor = navDetail.Value;
                        navDetail = nav.SelectSingleNode("(//r/jt)[" + j + "]");
                        myjob.jobtitle = navDetail.Value;
                        navDetail = nav.SelectSingleNode("(//r/loc)[" + j + "]");
                        myjob.loc = navDetail.Value;
                        navDetail = nav.SelectSingleNode("(//r/e)[" + j + "]");
                        myjob.jobdesc = navDetail.Value;
                        navDetail = nav.SelectSingleNode("(//r/dp)[" + j + "]");
                        myjob.dp = navDetail.Value;
                        jobslist.Add(myjob);
                        
                    }
                }
                catch (Exception ex)
                {
                    //log error
                }

                return jobslist;
            }

        }
    }
}