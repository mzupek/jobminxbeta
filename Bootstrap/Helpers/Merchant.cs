﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.ServiceModel;
using System.Web.Services;
using System.ServiceModel.Description;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.IO;
using System.Net;


namespace Bootstrap.Helpers
{
    public class Merchant
    {
        
        public MerchantResponse charge(string cardholdersname, string cardnumber, string exdate, string total)
        {
            string AuthNum = "";
            string ammount = "";
            string exDate = "";
            string ctr = "";
            bool approved = false;
            var TransRes = new MerchantResponse();
            StringBuilder string_builder = new StringBuilder();
            using (StringWriter string_writer = new StringWriter(string_builder))
            {
                using (XmlTextWriter xml_writer = new XmlTextWriter(string_writer))
                {     //build XML string 
                    xml_writer.Formatting = Formatting.Indented;
                    xml_writer.WriteStartElement("Transaction");
                    xml_writer.WriteElementString("ExactID", "A88688-01");//Gateway ID
                    xml_writer.WriteElementString("Password", "kpx0892d");//Password
                    xml_writer.WriteElementString("Transaction_Type", "00");
                    xml_writer.WriteElementString("DollarAmount", total);
                    xml_writer.WriteElementString("Expiry_Date", exdate);
                    xml_writer.WriteElementString("CardHoldersName", cardholdersname);
                    xml_writer.WriteElementString("Card_Number", cardnumber);
                    xml_writer.WriteEndElement();
                }
            }
            string xml_string = string_builder.ToString();

            //SHA1 hash on XML string
            ASCIIEncoding encoder = new ASCIIEncoding();
            byte[] xml_byte = encoder.GetBytes(xml_string);
            SHA1CryptoServiceProvider sha1_crypto = new SHA1CryptoServiceProvider();
            string hash = BitConverter.ToString(sha1_crypto.ComputeHash(xml_byte)).Replace("-", "");
            string hashed_content = hash.ToLower();

            //assign values to hashing and header variables
            string keyID = "84029";//key ID
            string key = "gWxTw4SJIa2U5wiWTxwKzds44NoPkzQR";//Hmac key
            string method = "POST\n";
            string type = "application/xml";//REST XML
            string time = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
            string uri = "/transaction/v14";
            string hash_data = method + type + "\n" + hashed_content + "\n" + time + "\n" + uri;
            //hmac sha1 hash with key + hash_data
            HMAC hmac_sha1 = new HMACSHA1(Encoding.UTF8.GetBytes(key)); //key
            byte[] hmac_data = hmac_sha1.ComputeHash(Encoding.UTF8.GetBytes(hash_data)); //data
            //base64 encode on hmac_data
            string base64_hash = Convert.ToBase64String(hmac_data);
            string url = "https://api.globalgatewaye4.firstdata.com" + uri; //DEMO Endpoint

            //begin HttpWebRequest 
            HttpWebRequest web_request = (HttpWebRequest)WebRequest.Create(url);
            web_request.Method = "POST";
            web_request.ContentType = type;
            web_request.Accept = "*/*";
            web_request.Headers.Add("x-gge4-date", time);
            web_request.Headers.Add("x-gge4-content-sha1", hashed_content);
            web_request.Headers.Add("Authorization", "GGE4_API " + keyID + ":" + base64_hash);
            web_request.ContentLength = xml_string.Length;

            // write and send request data 
            using (StreamWriter stream_writer = new StreamWriter(web_request.GetRequestStream()))
            {
                stream_writer.Write(xml_string);
            }
            
            //get response and read into string
            string response_string;
            try
            {
                using (HttpWebResponse web_response = (HttpWebResponse)web_request.GetResponse())
                {
                    using (StreamReader response_stream = new StreamReader(web_response.GetResponseStream()))
                    {
                        response_string = response_stream.ReadToEnd();
                    }

                    //load xml
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(response_string);
                    XmlElement root = xmldoc.DocumentElement;
                    XmlNodeList nodelist = xmldoc.SelectNodes("TransactionResult");
                    XmlNodeList _authNum = root.GetElementsByTagName("Authorization_Num");
                    AuthNum = _authNum.Item(0).InnerXml;
                    XmlNodeList _exDate = root.GetElementsByTagName("Expiry_Date");
                    exdate = _exDate.Item(0).InnerXml;
                    XmlNodeList _ammount = root.GetElementsByTagName("DollarAmount");
                    ammount = _ammount.Item(0).InnerXml;
                    XmlNodeList _approved = root.GetElementsByTagName("Transaction_Approved");
                    approved = Convert.ToBoolean(_approved.Item(0).InnerXml);
                    XmlNodeList _ctr = root.GetElementsByTagName("CTR");
                    ctr = _ctr.Item(0).InnerXml;
                    
                     

                    //output raw XML for debugging

                    var request= "<b>Request</b><br />" + web_request.Headers.ToString() + System.Web.HttpUtility.HtmlEncode(xml_string);
                    var response = "<b>Response</b><br />" + web_response.Headers.ToString() + System.Web.HttpUtility.HtmlEncode(response_string);
                    TransRes.IsTransactionApproved = approved;
                    TransRes.Message = response;
                    TransRes.IsError = false;
                    TransRes.authNum = AuthNum;
                    TransRes.exDate = exdate;
                    TransRes.ammount = ammount;
                    TransRes.ctr = ctr;

                }
            }
            //read stream for remote error response
            catch (WebException ex)
            {
                if (ex.Response != null)
                {
                    using (HttpWebResponse error_response = (HttpWebResponse)ex.Response)
                    {
                        using (StreamReader reader = new StreamReader(error_response.GetResponseStream()))
                        {
                            string remote_ex = reader.ReadToEnd();
                            var error = remote_ex;
                            TransRes.IsTransactionApproved = approved;
                            TransRes.Message = remote_ex;
                            TransRes.IsError = true;
                            

                        }
                    }
                }
            }

            return TransRes;
        }

    }

    public class MerchantResponse
    {
        public bool IsTransactionApproved { get; set; }
        public string authNum {get;set;}
        public string ctr {get;set;}
        public string exDate {get;set;}
        public string ammount {get;set;}
        public bool IsError { get; set; }
        public string Message { get; set; }
    }
}




