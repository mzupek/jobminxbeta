﻿using Bootstrap.com.resumeparsing.services1;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;
using System.Xml.XPath;
using Bootstrap.Models;

namespace Bootstrap.Helpers
{
    public class SovrenJob
    {
        public string GetJobXml(string job)
        {
            // Change global setting to make POST requests in one round-trip instead of two.
            System.Net.ServicePointManager.Expect100Continue = false;

            // Instantiate the web service client that was created in Visual Studio via "Add Web Reference"
            ParsingService service = new ParsingService();

            // Allow the response to be compressed
            service.EnableDecompression = false;

            ParseJobOrderRequest request = new ParseJobOrderRequest
            {
                AccountId = "42848698",
                ServiceKey = "o2HTFboiSqprZbKsFBuszZAadbnYXxfoMW21j4EL",
                FileText = job,


            };


            ParseJobOrderResponse returned = new ParseJobOrderResponse();

            returned = service.ParseJobOrder(request);
            
            int usesleft = returned.CreditsRemaining;
            ApplicationDbContext db = new ApplicationDbContext();
            var q = db.apis.Find(1);
            q.Worth = usesleft;
            db.SaveChanges();

            return returned.Xml;
        }

        public Job GetJobArray(string xml)
        {

            Job jobparse = new Job(); 
            string bestTax = "";
            //List<string> values = new List<string>();
            Dictionary<string, string> values = new Dictionary<string, string>();
            // Scrub XML namespaces so XPath is simpler to write
            xml = xml.Replace("xmlns=\"http://ns.hr-xml.org/2006-02-28\"", "");
            xml = xml.Replace("<sov:", "<");
            xml = xml.Replace("</sov:", "</");

            XPathDocument doc = new XPathDocument(new StringReader(xml));
            //XDocument doc = XDocument.Load(HttpContext.Current.Server.MapPath("/Helpers/jobsXML.xml"));
            XPathNavigator nav = doc.CreateNavigator();
            //get job title//
            try
            {
                XPathNavigator navDetail = nav.SelectSingleNode("//JobTitles");
                values["JobTitle"] = (GetValue(navDetail, "MainJobTitle"));
            }
            catch(Exception ex)
            {
                values["JobTitle"] = "Not Available";

            }
            //Taxonomy
            try
            {
                XPathNavigator navDetail = nav.SelectSingleNode("//Taxonomies/Taxonomy");
                //values["BestFitTax"] = (GetValue(navDetail, "ParentTaxonomyName"));
                bestTax = (GetValue(navDetail, "ParentTaxonomyName"));

            }
            catch(Exception ex)
            {
                values["BestFitTax"] = "Not Available";
            }

            //List of parent Taxonomies
            List<string> Taxes = new List<string>();
            try
            {
                XPathNavigator navTax = nav.SelectSingleNode("//Taxonomies/Taxonomy");
                XPathNodeIterator Tax = navTax.Select("//*");

                int count = Tax.Current.SelectChildren(XPathNodeType.Element).Count;
                if (count > 0)
                {
                    for (int j = 1; j <=2; j++)
                    {
                        navTax = nav.SelectSingleNode("(//ParentTaxonomyName)[" + j + "]");
                        string Taxonomy = navTax.Value;
                        Taxes.Add(Taxonomy);
                    }
                }

                XPathNavigator navTax2 = nav.SelectSingleNode("//Taxonomies/Taxonomy");
                XPathNodeIterator Tax2 = navTax2.Select("//*");

                int count2 = Tax2.Current.SelectChildren(XPathNodeType.Element).Count;
                if (count2 > 0)
                {
                    for (int j = 1; j <= 2; j++)
                    {
                        navTax2 = nav.SelectSingleNode("(//SubTaxonomyName)[" + j + "]");
                        string Taxonomy = navTax2.Value;
                        Taxes.Add(Taxonomy);
                    }
                }
            }
            catch (Exception ex)
            {

                //log error
            }

            //MinimumYears
            try
            {
                string minimum = nav.SelectSingleNode("//MinimumYears").Value;
                values["MinimumYears"] = minimum;
            }
            catch(Exception ex)
            {

                //log error
                values["MinimumYears"] = "Not Available";

            }
            

            //MinimumYearsManagement
            try
            {
                string MinimumYearsManagement = nav.SelectSingleNode("//MinimumYearsManagement").Value;
                values["MinimumYearsManagement"] = MinimumYearsManagement;
            }
            catch(Exception ex)
            {
                //log error
                values["MinimumYearsManagement"] = "Not Available";
            }
            

            //MaximumYearsManagement
            try
            {
                string MaximumYearsManagement = nav.SelectSingleNode("//MaximumYearsManagement").Value;
                values["MaximumYearsManagement"] = MaximumYearsManagement;
            }
            catch(Exception ex)
            {
                //log error
                values["MaximumYearsManagement"] = "Not Available";

            }
           
            
            //RequiredDegree
            try
            {
                string RequiredDegree = nav.SelectSingleNode("//RequiredDegree").Value;
                values["RequiredDegree"] = RequiredDegree;
            }
            catch(Exception ex)
            {

                values["RequiredDegree"] = "Not Available";
            }
            
            
            //get certiifcations and Licenses//
            List<string> CL = new List<string>();
            try
            {
                XPathNavigator navCL = nav.SelectSingleNode("//CertificationsAndLicenses");
                XPathNodeIterator CLS = navCL.Select("//*");

                int count = CLS.Current.SelectChildren(XPathNodeType.Element).Count;
                if (count > 0)
                {
                    for (int j = 1; j <= count; j++)
                    {
                        navCL = nav.SelectSingleNode("(//CertificationOrLicense)[" + j + "]");
                        string CertLic = navCL.Value;
                        CL.Add(CertLic);
                    }
                }
            }
            catch(Exception ex)
            {

                //log error
            }
            

            //Get Required Skills
            
            List<string> SKL = new List<string>();
            try
            {
                XPathNavigator navSk = nav.SelectSingleNode("//RequiredSkills");
                XPathNodeIterator Sk = navSk.Select("//*");


                int count2 = Sk.Current.SelectChildren(XPathNodeType.Element).Count;

                for (int i = 1; i <= count2; i++)
                {
                    navSk = nav.SelectSingleNode("(//RequiredSkill)[" + i + "]");
                    string skills = navSk.Value;
                    SKL.Add(skills);
                }
            }
            catch(Exception ex)
            {

                //log error
            }
           

            //Other Skills - OtherSkills
            List<string> Other = new List<string>();

            try
            {
                XPathNavigator navOS = nav.SelectSingleNode("//OtherSkills");
                XPathNodeIterator OS = navOS.Select("//*");


                int countO = OS.Current.SelectChildren(XPathNodeType.Element).Count;

                for (int b = 1; b <= countO; b++)
                {
                    navOS = nav.SelectSingleNode("(//OtherSkill)[" + b + "]");
                    string otherSkills = navOS.Value;
                    Other.Add(otherSkills);
                }
            }
            catch(Exception ex)
            {

               //log error
            }
           

            //Get Education

            
            List<Dictionary<string, string>> EDU = new List<Dictionary<string, string>>();
            Dictionary<string, string> degree = new Dictionary<string, string>();
            try
            {
                XPathNavigator navEd = nav.SelectSingleNode("//Education");
                XPathNodeIterator Ed = navEd.Select("//*");
                int count3 = Ed.Current.SelectChildren(XPathNodeType.Element).Count;

                for (int r = 1; r <= count3; r++)
                {
                    navEd = nav.SelectSingleNode("(//Degree)[" + r + "]");
                    string Types = navEd.SelectSingleNode("//DegreeType").Value;
                    string Name = navEd.SelectSingleNode("//DegreeName").Value;
                   
                    degree["Type"] = Types;
                    degree["Name"] = Name;
                    
                }
            }
            catch(Exception ex)
            {
                //log error
                degree["Type"] = "Not Available";
                degree["Name"] = "Not Available";
            }
            
            EDU.Add(degree);

            //Get Years of Experinece
            jobparse.SingleValues = values;
            jobparse.CertificationOrLicenes = CL;
            jobparse.Education = EDU;
            jobparse.Skills = SKL;
            jobparse.OtherSkills = Other;
            jobparse.BestTax = bestTax;
            jobparse.Taxonomies = Taxes;

            return jobparse;
        }

        private void AppendCsvValue(StringBuilder sb, string text)
        {
            if (!String.IsNullOrEmpty(text))
            {
                // Change line breaks into spaces
                text = text.Replace("\r\n", " ");
                text = text.Replace("\r", " ");
                text = text.Replace("\n", " ");

                // Escape quotes by doubling them
                text = text.Replace("\"", "\"\"");

                // Wrap the final value in quotes
                sb.Append('"').Append(text).Append('"');
            }
        }

        private string GetValue(XPathNavigator nav, string xpath)
        {
            if (nav == null) return String.Empty;

            nav = nav.SelectSingleNode(xpath);
            if (nav == null)
            {
                return String.Empty;
            }
            return nav.Value.ToUpper();
        }

        private string GetValues(XPathNavigator nav, string xpath)
        {
            if (nav == null) return String.Empty;

            StringBuilder sb = new StringBuilder(100);
            XPathNodeIterator iter = nav.Select(xpath);
            while (iter.MoveNext())
            {
                if (sb.Length > 0)
                {
                    sb.Append(", ");
                }
                sb.Append(iter.Current.Value);
            }
            return sb.ToString().ToUpper();
        }
    }
}