﻿using Bootstrap.Api;
using Bootstrap.Api.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bootstrap.Helpers
{
    public class JobParse
    {
        public DocumentResult GetResults(string inputText)
        {
            //var inputText = "I am not negative about the LG brand.";
            string privateKey = "19037115-07AF-4BC9-A8FF-B9FEB888660E"; //add your private key here (you can find it in the admin panel once you sign-up)
            string secret = "jobminx"; //this should be set-up in admin panel as well
            /////////////////////////

            #region Create request object
            var doc = new Document()
                {
                    DocumentText = inputText,
                    IsTwitterContent = false,
                    PrivateKey = privateKey,
                    Secret = secret
                };
            #endregion

            var docResult = API.GetDocumentAnalysisResult(doc); //execute request

            if (docResult.Status == (int)DocumentResultStatus.OK) //check status
            {
                #region Display results
                //display document level score
                Console.WriteLine(string.Format("This document is: {0}{1} {2}", docResult.DocSentimentPolarity, docResult.DocSentimentResultString, docResult.DocSentimentValue.ToString("0.000")));

                #region display entity scores if any found
                if (docResult.Entities != null && docResult.Entities.Any())
                {
                    Console.WriteLine(Environment.NewLine + "Entities:");
                    foreach (var entity in docResult.Entities)
                    {
                        Console.WriteLine(string.Format("{0} ({1}) {2}{3} {4}", entity.Text, entity.KeywordType, entity.SentimentPolarity, entity.SentimentResult, entity.SentimentValue.ToString("0.0000")));
                    }
                }
                #endregion

                #region display keyword scores if any found

                if (docResult.Keywords != null && docResult.Keywords.Any())
                {
                    Console.WriteLine(Environment.NewLine + "Keywords:");
                    foreach (var keyword in docResult.Keywords)
                    {
                        Console.WriteLine(string.Format("{0} {1}{2} {3}", keyword.Text, keyword.SentimentPolarity, keyword.SentimentResult, keyword.SentimentValue.ToString("0.0000")));
                    }
                }
                #endregion


                //display more information below if required 

                #endregion
            }
            else
            {
                Console.WriteLine(docResult.ErrorMessage);
            }

            return docResult;
        }
    }
}