﻿using Bootstrap.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace Bootstrap.Helpers
{
    public class Messenger
    {
        public string SendContactConfirmation(string toAddress)
        {


            MailMessage mail = new MailMessage();
            string returnMess;
            try
            {
                var c = HttpContext.Current;


                SmtpClient SmtpServer = new SmtpClient("smtp.mandrillapp.com", 587);
                NetworkCredential basicCredential = new NetworkCredential("mzupek@gmail.com", "zZAw01G5tLD5A89RxXNbXw");
                SmtpServer.Credentials = basicCredential;
                mail.From = new MailAddress("noreply@jobminx.com");
                mail.To.Add(toAddress);
                mail.Subject = "Thank you for contacting Jobminx.com";
                mail.IsBodyHtml = true;
                mail.Body = "<p>Thank you for contacting Jobminx.com, We have revieved you message and will respond with in the next business day. </br> Thanks, </b>Jobminx.com </p>";
                try
                {
                    SmtpServer.Send(mail);
                    returnMess = "200";
                }
                catch(Exception ex)
                {
                    returnMess = ex.Message;
                }
               
                






            }
            catch (Exception ex)
            {
                returnMess = ex.Message;

            }


            return returnMess;

        }



        public string SendInfoMessage(string toAddress, string firstname, string message)
        {


            MailMessage mail = new MailMessage();
            string returnMess;
            try
            {
                var c = HttpContext.Current;


                SmtpClient SmtpServer = new SmtpClient("smtp.mandrillapp.com", 587);
                NetworkCredential basicCredential = new NetworkCredential("mzupek@gmail.com", "zZAw01G5tLD5A89RxXNbXw");
                SmtpServer.Credentials = basicCredential;
                mail.From = new MailAddress("noreply@jobminx.com");
                mail.To.Add("info@jobminx.com");
                mail.Subject = "A message has been submitted to Jobminx.com";
                mail.IsBodyHtml = true;
                mail.Body = "<p>"+firstname+"</p></br>";
                mail.Body += "<p>" + toAddress + "</p></br>";
                mail.Body += "<p>" + message + "</p>";
                try
                {
                    SmtpServer.Send(mail);
                    returnMess = "200";
                }
                catch (Exception ex)
                {
                    returnMess = ex.Message;
                }








            }
            catch (Exception ex)
            {
                returnMess = ex.Message;

            }


            return returnMess;

        }

        public string SendPasswordReset(string toAddress, string token)
        {


            MailMessage mail = new MailMessage();
            string returnMess;
            try
            {
                var c = HttpContext.Current;


                SmtpClient SmtpServer = new SmtpClient("smtp.mandrillapp.com", 587);
                NetworkCredential basicCredential = new NetworkCredential("mzupek@gmail.com", "zZAw01G5tLD5A89RxXNbXw");
                SmtpServer.Credentials = basicCredential;
                string resetlink = "https://jobminx.com/Account/ResetYourPassword?token=" + token;
                mail.From = new MailAddress("noreply@jobminx.com");
                mail.To.Add(toAddress);
                mail.Subject = "Jobminx.com Password Reset";
                mail.IsBodyHtml = true;
                mail.Body = "<p>Click the link below to reset your password</p></br>";
                mail.Body += "<p><a href='"+resetlink+"'>Reset Your Password</a></p></br></br>";
                mail.Body += "<p>thank you,</p>";
                mail.Body += "<p>Team Jobminx</p>";
                try
                {
                    SmtpServer.Send(mail);
                    returnMess = "200";
                }
                catch (Exception ex)
                {
                    returnMess = ex.Message;
                }








            }
            catch (Exception ex)
            {
                returnMess = ex.Message;

            }


            return returnMess;

        }

        public string SendWelcomeMessage(PurchaseSummary trans, string NameOnCard)
        {


            MailMessage mail = new MailMessage();
            string returnMess;
            try
            {
                var c = HttpContext.Current;

                string[] plans = { "NA","Trial", "Monthly", "Extended" };
                string[] Details = { "NA", "Free - 5", "Monthly (One Month)", "Extended (Three Months)" };
                SmtpClient SmtpServer = new SmtpClient("smtp.mandrillapp.com", 587);
                NetworkCredential basicCredential = new NetworkCredential("mzupek@gmail.com", "zZAw01G5tLD5A89RxXNbXw");
                SmtpServer.Credentials = basicCredential;
                mail.From = new MailAddress("noreply@jobminx.com");
                mail.To.Add(trans.email);
                mail.Bcc.Add("info@jobminx.com");
                mail.Subject = "Welcome to Jobminx";
                mail.IsBodyHtml = true;
                mail.Body = "<div style='float:right;'><img src='https://jobminx.com/Content/jobminx_logo_final_small.png'/></div>";
                mail.Body += "<h3>It's Time to Get Hired!</h1>";
                mail.Body += "<h4>You are ready to target your resume and match it to any job description</h4>";
                mail.Body += "<p><a href='https://jobminx.com/Account/Login'>Start Now!</p>";
                mail.Body += "<p><b>Plan:</b>" +trans.plan +" Plan</p></br>";
                if(trans.tokens == 0)
                {
                    mail.Body += "<h4> Plan Details: Optimize and Target Unlimited Resumes and Access our Keyword Optimiztion Tool!</h4></br>";
                }
                else
                {
                    mail.Body += "<h4> Plan Details: Optimize and Target " + trans.tokens.ToString() + " Resumes!</h4></br>";
                }
                
                mail.Body += "<h3>Payment Information</h3>";
                mail.Body += "<p><b>Name: </b>"+NameOnCard+"</p>";
                mail.Body += "<p><b>Amount Billed:</b> $" + trans.total + "</p>";
                mail.Body += "<p><b>Confirmation Number:</b> " + trans.orderNumber+ "</p>";
                mail.Body += "<p><b>Order#:</b> " + trans.orderNumber + "</p><br/>";

                mail.Body += "<p> Questions about your order? <a href='https://jobminx.com/Start/Contact'>Contact Us</a></p>";
                try
                {
                    SmtpServer.Send(mail);
                    returnMess = "200";
                }
                catch (Exception ex)
                {
                    returnMess = ex.Message;
                }








            }
            catch (Exception ex)
            {
                returnMess = ex.Message;

            }


            return returnMess;

        }

        public string SendWelcomeMessageFree(PurchaseSummary trans)
        {


            MailMessage mail = new MailMessage();
            string returnMess;
            try
            {
                var c = HttpContext.Current;

                string[] plans = { "NA", "Trial", "Monthly", "Extended" };
                string[] Details = { "NA", "Free - 5", "Monthly (One Month)", "Extended (Three Months)" };
                SmtpClient SmtpServer = new SmtpClient("smtp.mandrillapp.com", 587);
                NetworkCredential basicCredential = new NetworkCredential("mzupek@gmail.com", "zZAw01G5tLD5A89RxXNbXw");
                SmtpServer.Credentials = basicCredential;
                mail.From = new MailAddress("noreply@jobminx.com");
                mail.To.Add(trans.email);
                mail.Bcc.Add("info@jobminx.com");
                mail.Subject = "Welcome to Jobminx";
                mail.IsBodyHtml = true;
                mail.Body = "<div style='float:right;'><img src='https://jobminx.com/Content/jobminx_logo_final_small.png'/></div>";
                mail.Body += "<h3>It's Time to Get Hired!</h1>";
                mail.Body += "<h4>You are ready to target your resume and match it to any job description</h4>";
                mail.Body += "<p><a href='https://jobminx.com/Account/Login'>Start Now!</a></p>";
                mail.Body += "<p><b>Plan:</b>" + trans.plan + " Plan</p></br>";
                if (trans.tokens == 0)
                {
                    mail.Body += "<h4> Plan Details: Optimize and Target Unlimited Resumes and Access our Keyword Optimiztion Tool!</h4></br>";
                }
                else
                {
                    mail.Body += "<h4> Plan Details: Optimize and Target " + trans.tokens.ToString() + " Resumes!</h4></br>";
                }

                

                mail.Body += "<p> Questions about your order? <a href='https://jobminx.com/Start/Contact'>Contact Us</a></p>";
                try
                {
                    SmtpServer.Send(mail);
                    returnMess = "200";
                }
                catch (Exception ex)
                {
                    returnMess = ex.Message;
                }








            }
            catch (Exception ex)
            {
                returnMess = ex.Message;

            }


            return returnMess;

        }


    }
}