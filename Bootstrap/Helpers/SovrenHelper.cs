﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Configuration;
using System.Xml;
using System.IO;
using Bootstrap.Models;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Diagnostics;
using System.Globalization;
using System.IO.Compression;
using Bootstrap.com.resumeparsing.services;
using HtmlAgilityPack;
using System.Text.RegularExpressions;



namespace Bootstrap.Helpers
{
    public class SovrenHelper
    {
        public static ResumeService _resumeService;
        public string Status;
        public ResumeService GetResumeService()
        {
            if (_resumeService == null)
            {
                Status = "Creating the web service client proxy";

                // Change global setting to make POST requests in one round-trip instead of two.
                System.Net.ServicePointManager.Expect100Continue = false;

                // Instantiate the web service client that was created in Visual Studio via "Add Web Reference"
                ResumeService service = new ResumeService();

                // Allow the response to be compressed
                service.EnableDecompression = false;
                

                _resumeService = service;
            }

            return _resumeService;
        }



        public byte[] convertToBytes(string input)
        {

            byte[] bytes = new byte[input.Length * sizeof(char)];
            System.Buffer.BlockCopy(input.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public string GetConfiguration()
        {
            // All settings are optional. The default settings are appropriate for many applications,
            // but we encourage you to review the available options to determine the configuration
            // that is best suited to your needs. See the Parser Config String Builder.xls spreadsheet
            // for full details.

            // In the code below, we subtract one from each offset because the spreadsheet uses
            // one-based offsets and C# uses zero-based offsets. The "sb[NN - 1]" syntax reduces
            // human error in looking up and setting these configuration parameters.

            // Load default configuration string from the .config file.
            string configuration = ConfigurationManager.AppSettings["Jobminx_Service_ResumeService"] ?? String.Empty;
            
            // Extend the configuration to length of 100, so we can override some settings.
            StringBuilder sb = new StringBuilder(configuration);
            for (int i = configuration.Length; i < 100; i++)
            {
                sb.Append('_');
            }

            // Then we set individual options...

            sb[31 - 1] = '1';  // ExplicitlyKnownDateInfoOnly

            // Set ContactMethodPackStyle to Packed or PackedNoOverflow in order to consolidate
            // contact information into as few ContactMethod elements as possible,
            // grouped by home vs. work.
            //
            //sb[33 - 1] = '2';  // ContactMethodPackStyle

            // Set culture used for parsing, if needed. These values come from the _cultures
            // list that we defined for the ComboBox items.
            string culture = "Auto Detect";
            switch (culture)
            {
                case "Auto Detect": break;
                case "Argentina": sb[80 - 1] = '1'; break;
                case "Australia": sb[3 - 1] = '1'; break;
                case "Austria": sb[40 - 1] = '1'; break;
                case "Belgium": sb[87 - 1] = '1'; break;
                case "Brazil": sb[89 - 1] = '1'; break;
                case "Canada": break;
                case "China": sb[91 - 1] = '1'; break;
                case "Czech Republic": sb[86 - 1] = '1'; break;
                case "France": sb[7 - 1] = '1'; break;
                case "Germany": sb[40 - 1] = '1'; break;
                case "Greece": sb[81 - 1] = '1'; break;
                case "Hungary": sb[82 - 1] = '1'; break;
                case "India": sb[7 - 1] = '1'; break;
                case "Ireland": sb[5 - 1] = '1'; break;
                case "Italy": sb[83 - 1] = '1'; break;
                case "Liechtenstein": sb[40 - 1] = '1'; break;
                case "Mexico": sb[88 - 1] = '1'; break;
                case "Netherlands": sb[39 - 1] = '1'; break;
                case "New Zealand": sb[2] = '1'; break;
                case "Norway": sb[85 - 1] = '1'; break;
                case "Pakistan": sb[7 - 1] = '1'; break;
                case "Portugal": sb[90 - 1] = '1'; break;
                case "Russia": sb[84 - 1] = '1'; break;
                case "South Africa": sb[41 - 1] = '1'; break;
                case "Spain": sb[73 - 1] = '1'; break;
                case "Sweden": sb[61 - 1] = '1'; break;
                case "Switzerland": sb[40 - 1] = '1'; break;
                case "United States": break;
                case "United Kingdom": sb[4 - 1] = '1'; break;
            }


            sb[9 - 1] = '0'; // SkipAchievementsParsing
            sb[11 - 1] = '0'; // SkipAssociationsParsing
            sb[12 - 1] = '0'; // SkipEducationParsing
            sb[13 - 1] = '0'; // SkipEmploymentParsing
            sb[14 - 1] = '0'; // SkipJobObjectiveParsing
            sb[15 - 1] = '0'; // SkipLanguagesParsing
            sb[16 - 1] = '0'; // SkipLicensesAndCertificationsParsing
            sb[17 - 1] = '0'; // SkipManagementParsing
            sb[18 - 1] = '0'; // SkipMilitaryHistoryParsing
            sb[20 - 1] = '0'; // SkipPatentsParsing
            sb[21 - 1] = '0'; // SkipPublicationsParsing
            sb[22 - 1] = '0'; // SkipReferencesParsing
            sb[23 - 1] = '0'; // SkipSecurityCredentialsParsing
            sb[24 - 1] = '0'; // SkipSkillsParsing
            sb[25 - 1] = '0'; // SkipSpeakingEventsParsing
            sb[26 - 1] = '0'; // SkipSummariesParsing
            sb[27 - 1] = '0'; // SkipJobCategoryParsing

            // These options default to true. Set to false to prevent parsing.

            sb[9 - 1] = '0'; // SkipAchievementsParsing
            sb[11 - 1] = '0'; // SkipAssociationsParsing
            sb[12 - 1] = '0'; // SkipEducationParsing
            sb[13 - 1] = '0'; // SkipEmploymentParsing
            sb[14 - 1] = '0'; // SkipJobObjectiveParsing
            sb[15 - 1] = '0'; // SkipLanguagesParsing
            sb[16 - 1] = '0'; // SkipLicensesAndCertificationsParsing
            sb[17 - 1] = '0'; // SkipManagementParsing
            sb[22 - 1] = '0'; // SkipReferencesParsing
            sb[24 - 1] = '0'; // SkipSkillsParsing
            sb[26 - 1] = '0'; // SkipSummariesParsing

            // These options default to false. Set to true to enable parsing.

            sb[18 - 1] = '0'; // SkipMilitaryHistoryParsing
            sb[20 - 1] = '0'; // SkipPatentsParsing
            sb[21 - 1] = '0'; // SkipPublicationsParsing
            sb[23 - 1] = '0'; // SkipSecurityCredentialsParsing
            sb[25 - 1] = '0'; // SkipSpeakingEventsParsing
            sb[27 - 1] = '0'; // SkipJobCategoryParsing

            // Personal information. These options default to false. Set to true to enable parsing.
            // You can set these in batch as shown, or you can set them individually.


            sb[42 - 1] = '0'; // ParseDateOfBirth
            sb[43 - 1] = '0'; // ParseMaritalStatus
            sb[44 - 1] = '0'; // ParseGender
            sb[45 - 1] = '0'; // ParseNationality
            sb[46 - 1] = '0'; // ParseVisa
            sb[47 - 1] = '0'; // ParsePassport
            sb[48 - 1] = '0'; // ParseLocation
            sb[49 - 1] = '0'; // ParseDrivingLicense
            sb[50 - 1] = '0'; // ParseSalary
            sb[51 - 1] = '0'; // ParseAncestor
            sb[62 - 1] = '0'; // ParseTraining
            sb[66 - 1] = '0'; // ParseNationalIdentityNumber
            sb[69 - 1] = '0'; // ParseBirthplace
            sb[70 - 1] = '0'; // ParseFamilyComposition
            sb[93 - 1] = '0'; // ParseHukou
            sb[94 - 1] = '0'; // ParsePolitics
            sb[95 - 1] = '0'; // ParseMessagingAddresses



            // When writing the skills to the HR-XML results, show the parent skills in addition to the child skills that were found.
            sb[2 - 1] = '0';

            // Set true if you want skill parsing to be case-insensitive, regardless of the per-skill case-sensitivity setting.
            // Beware of false positive matches, such as "progress".
            //
            // sb[68 - 1] = '1';

            return sb.ToString();
        }
        public List<Dictionary<string,string>> GetExperience (string xml)
        {
            // Scrub XML namespaces so XPath is simpler to write
            xml = xml.Replace("xmlns=\"http://ns.hr-xml.org/2006-02-28\"", "");
            xml = xml.Replace("<sov:", "<");
            xml = xml.Replace("</sov:", "</");

            XPathDocument doc = new XPathDocument(new StringReader(xml));
            XPathNavigator nav = doc.CreateNavigator();
            XPathNavigator navDetail;
            List<Dictionary<string, string>> experience = new List<Dictionary<string, string>>();
            for (int i = 1; i <= 5; i++)
            {
                Dictionary<string, string> values = new Dictionary<string, string>();
                navDetail = nav.SelectSingleNode("(//PositionHistory)[" + i + "]");
                string orgName = GetValue(navDetail, "../EmployerOrgName");
                values["OrganizationName"] = (orgName);
                string subOrgName = GetValue(navDetail, "OrgName/OrganizationName");
                values["OrgName"] = (subOrgName == orgName ? String.Empty : subOrgName);
                string location = GetValue(navDetail, "PositionLocation/CountryCode")
                    + "-" + GetValue(navDetail, "PositionLocation/Region")
                    + "-" + GetValue(navDetail, "PositionLocation/Municipality");
                values["OrgLocation"] = (location == "--" ? String.Empty : location);
                values["Title"] = (GetValue(navDetail, "Title"));
                string startDate = GetValue(navDetail, "StartDate");
                values["OrgStartDate"] = (startDate == "notKnown" ? String.Empty : startDate);
                string endDate = GetValue(navDetail, "EndDate");
                values["OrgEndDate"] = (endDate == "notKnown" ? String.Empty : endDate);
                experience.Add(values);
            }
            return experience;
        }

        public List<string> GetEducation(string xml)
        {
            // Scrub XML namespaces so XPath is simpler to write
            xml = xml.Replace("xmlns=\"http://ns.hr-xml.org/2006-02-28\"", "");
            xml = xml.Replace("<sov:", "<");
            xml = xml.Replace("</sov:", "</");

            XPathDocument doc = new XPathDocument(new StringReader(xml));
            XPathNavigator nav = doc.CreateNavigator();
            XPathNavigator navDetail;
            List<Dictionary<string, string>> education = new List<Dictionary<string, string>>();
            List<string> Degrees = new List<string>();
            try
            {

                XPathNavigator navEd = nav.SelectSingleNode("//EducationHistory");
                XPathNodeIterator Ed = navEd.Select("//*");

               
                int countO = Ed.Current.SelectChildren(XPathNodeType.Element).Count;


                for (int i = 1; i <= countO; i++)
                {
                    //Dictionary<string, string> values = new Dictionary<string, string>();

                    //navDetail = 
                    //values["SchoolName" + i] = (GetValue(navDetail, "School/SchoolName"));
                    //values["SchoolType" + i] = (GetValue(navDetail, "@schoolType"));
                    //string location = GetValue(navDetail, "PostalAddress/CountryCode")
                    //  + "-" + GetValue(navDetail, "PostalAddress/Region")
                    //  + "-" + GetValue(navDetail, "PostalAddress/Municipality");
                    //values["SchoolLocation" + i] = (location == "--" ? String.Empty : location);
                    //values["DegreeName" + i] = (GetValue(navDetail, "Degree/DegreeName"));
                    Degrees.Add(nav.SelectSingleNode("(//SchoolOrInstitution/Degree/DegreeName)[" + i + "]").Value);
                    //values["DegreeType" + i] = (GetValue(navDetail, "Degree/@degreeType"));
                    //values["MajorName" + i] = (GetValue(navDetail, "Degree/DegreeMajor/Name"));
                    //values["MinorName" + i] = (GetValue(navDetail, "Degree/DegreeMinor/Name"));
                    //string startDate = GetValue(navDetail, "Degree/DatesOfAttendance/StartDate");
                    //values["SchoolStartDate" + i] = (startDate == "notKnown" ? String.Empty : startDate);
                    //string endDate = GetValue(navDetail, "Degree/DatesOfAttendance/EndDate");
                    //values["SchoolEndDate" + i] = (endDate == "notKnown" ? String.Empty : endDate);

                    //education.Add(values);
                }
            }
            catch(Exception ex)
            {
                //log error
            }
            return Degrees;
        }

        public List<string> GetCompetencies (string xml)
        {
            // Scrub XML namespaces so XPath is simpler to write
            xml = xml.Replace("xmlns=\"http://ns.hr-xml.org/2006-02-28\"", "");
            xml = xml.Replace("<sov:", "<");
            xml = xml.Replace("</sov:", "</");

            XPathDocument doc = new XPathDocument(new StringReader(xml));
            XPathNavigator nav = doc.CreateNavigator();
            XPathNavigator navDetail;
            List<string> comps = new List<string>();
            try
            {

                XPathNavigator navQual = nav.SelectSingleNode("(//Qualifications)");
                XPathNodeIterator it = navQual.Select("//*");
                int count = it.Current.SelectChildren(XPathNodeType.Element).Count;
                for (int j = 1; j <= count - 1; j++)
                {
                    navDetail = nav.SelectSingleNode("(//Competency/@name)[" + j + "]");
                    string comp = navDetail.Value;
                    comps.Add(comp);
                }
            }
            catch(Exception ex)
            {
                //log error
            }
            return comps;
        }
       
        public List<string> GetBestFitTaxonomies(string xml)
        {

            List<string> taxes = new List<string>();
            // Scrub XML namespaces so XPath is simpler to write
            xml = xml.Replace("xmlns=\"http://ns.hr-xml.org/2006-02-28\"", "");
            xml = xml.Replace("<sov:", "<");
            xml = xml.Replace("</sov:", "</");

            XPathDocument doc = new XPathDocument(new StringReader(xml));
            XPathNavigator nav = doc.CreateNavigator();

            XPathNavigator navDetail = nav.SelectSingleNode("//BestFitTaxonomies");
            //BestFitTaxonomies
           
            XPathNodeIterator taxIt = navDetail.Select("//*");

               
                int count = taxIt.Current.SelectChildren(XPathNodeType.Element).Count;


                for (int ta = 1; ta <= count; ta++)
                {
                    //BestFitTaxonomy
                    navDetail = nav.SelectSingleNode("(//BestFitTaxonomy/@name)[" + ta + "]");
                    string t = navDetail.Value;
                    taxes.Add(t);
                }

                return taxes;

        }

        public Dictionary<string, string> GetResumeArray(string xml)
        {
            //List<string> values = new List<string>();
            Dictionary<string, string> values = new Dictionary<string, string>();
            // Scrub XML namespaces so XPath is simpler to write
            xml = xml.Replace("xmlns=\"http://ns.hr-xml.org/2006-02-28\"", "");
            xml = xml.Replace("<sov:", "<");
            xml = xml.Replace("</sov:", "</");

            XPathDocument doc = new XPathDocument(new StringReader(xml));
            XPathNavigator nav = doc.CreateNavigator();
            //values.Add(filename);

            XPathNavigator navDetail = nav.SelectSingleNode("//ContactInfo/PersonName");
            values["FormattedName"] = (GetValue(navDetail, "FormattedName"));
            values["GivenName"] = (GetValue(navDetail, "GivenName"));
            values["MiddleName"] = (GetValue(navDetail, "MiddleName"));
            values["FamilyName"] = (GetValue(navDetail, "FamilyName"));
            values["Affix"] = (GetValues(navDetail, "Affix"));

            XPathNavigator navQual = nav.SelectSingleNode("(//Qualifications)");
            values["QualificationSummary"] = (GetValue(navQual, "QualificationSummary"));

            navDetail = nav.SelectSingleNode("(//ContactInfo/ContactMethod/PostalAddress)[1]");
            values["CountryCode"] = (GetValue(navDetail, "CountryCode"));
            values["PostalCode"] = (GetValue(navDetail, "PostalCode"));
            values["Region"] = (GetValues(navDetail, "Region"));
            values["Municipality"] = (GetValue(navDetail, "Municipality"));
            values["DeliveryAddress/AddressLine[1]"] = (GetValue(navDetail, "DeliveryAddress/AddressLine[1]"));
            values["DeliveryAddress/AddressLine[2]"] = (GetValue(navDetail, "DeliveryAddress/AddressLine[2]"));
            values["DeliveryAddress/AddressLine[3]"] = (GetValue(navDetail, "DeliveryAddress/AddressLine[3]"));

            values["Telephone1"] = (GetValue(nav, "(//ContactInfo/ContactMethod/Telephone)[1]/FormattedNumber"));
            values["Telephone2"] = (GetValue(nav, "(//ContactInfo/ContactMethod/Telephone)[2]/FormattedNumber"));
            values["Mobile1"] = (GetValue(nav, "(//ContactInfo/ContactMethod/Mobile)[1]/FormattedNumber"));
            values["Mobile2"] = (GetValue(nav, "(//ContactInfo/ContactMethod/Mobile)[2]/FormattedNumber"));
            values["Fax1"] = (GetValue(nav, "(//ContactInfo/ContactMethod/Fax)[1]/FormattedNumber"));
            values["Fax2"] = (GetValue(nav, "(//ContactInfo/ContactMethod/Fax)[2]/FormattedNumber"));
            values["Email1"] = (GetValue(nav, "(//ContactInfo/ContactMethod/InternetEmailAddress)[1]"));
            values["Email2"] = (GetValue(nav, "(//ContactInfo/ContactMethod/InternetEmailAddress)[2]"));
            values["Url1"] = (GetValue(nav, "(//ContactInfo/ContactMethod/InternetWebAddress)[1]"));
            values["Url2"] = (GetValue(nav, "(//ContactInfo/ContactMethod/InternetWebAddress)[2]"));
            values["ExecutiveSummary"] = (GetValue(nav, "//ExecutiveSummary"));
            values["Objectives"] = (GetValue(nav, "//Objective"));
           
            navDetail = nav.SelectSingleNode("/Resume/UserArea/ResumeUserArea/ExperienceSummary");
            values["MonthsOfWorkExperince"] = (GetValue(navDetail, "MonthsOfWorkExperience"));
            values["MonthsOfManagementExperince"] = (GetValue(navDetail, "MonthsOfManagementExperience"));
            values["Description"] = (GetValue(navDetail, "Description"));

            navDetail = nav.SelectSingleNode("/Resume/UserArea/ResumeUserArea/PersonalInformation");
            values["DateOfBirth"] = (GetValue(navDetail, "DateOfBirth"));
            values["Nationality"] = (GetValue(navDetail, "Nationality"));
            values["Gender"] = (GetValue(navDetail, "Gender"));
            values["MaritalStatus"] = (GetValue(navDetail, "MaritalStatus"));
            values["DriversLicense"] = (GetValue(navDetail, "DrivingLicense"));
            values["CurrentLocation"] = (GetValue(navDetail, "CurrentLocation"));
            values["PreferredLocation"] = (GetValue(navDetail, "PreferredLocation"));
            values["WillingToRelocate"] = (GetValue(navDetail, "WillingToRelocate"));
            values["FathersName"] = (GetValue(navDetail, "FathersName"));
            values["MothersMadianName"] = (GetValue(navDetail, "MothersMaidenName"));
            values["Visa"] = (GetValue(navDetail, "Visa"));
            values["Passport"] = (GetValue(navDetail, "Passport"));
            values["CurrentSalary"] = (GetValue(navDetail, "CurrentSalary/@currency") + " " + GetValue(navDetail, "CurrentSalary"));
            values["RequiredSalary"] = (GetValue(navDetail, "RequiredSalary/@currency") + " " + GetValue(navDetail, "RequiredSalary"));

           // StringBuilder sb = new StringBuilder(2048);
           // for (int i = 0; i < values.Count; i++)
           // {
              //  if (i > 0)
              //  {
              //      sb.Append(',');
             //   }
              //  AppendCsvValue(sb, values[i]);
           // }

            return values;
        }

        public int ApiCalls()
        {

            int usesLeft = 10;
                
            return usesLeft;
            

        }

        private void AppendCsvValue(StringBuilder sb, string text)
        {
            if (!String.IsNullOrEmpty(text))
            {
                // Change line breaks into spaces
                text = text.Replace("\r\n", " ");
                text = text.Replace("\r", " ");
                text = text.Replace("\n", " ");

                // Escape quotes by doubling them
                text = text.Replace("\"", "\"\"");

                // Wrap the final value in quotes
                sb.Append('"').Append(text).Append('"');
            }
        }

        private string GetValue(XPathNavigator nav, string xpath)
        {
            if (nav == null) return String.Empty;

            nav = nav.SelectSingleNode(xpath);
            if (nav == null)
            {
                return String.Empty;
            }
            return nav.Value.ToUpper();
        }

        private string GetValues(XPathNavigator nav, string xpath)
        {
            if (nav == null) return String.Empty;

            StringBuilder sb = new StringBuilder(100);
            XPathNodeIterator iter = nav.Select(xpath);
            while (iter.MoveNext())
            {
                if (sb.Length > 0)
                {
                    sb.Append(", ");
                }
                sb.Append(iter.Current.Value);
            }
            return sb.ToString().ToUpper();
        }

       

        public string GetResumeXML(string text)
        {
            
            SovrenHelper getRef = new SovrenHelper();
            // Get the web service client
            ResumeService service = getRef.GetResumeService();

            

            ParseResumeRequest request = new ParseResumeRequest
            {
                AccountId = "42848698",
                ServiceKey = "o2HTFboiSqprZbKsFBuszZAadbnYXxfoMW21j4EL",
                Configuration = getRef.GetConfiguration(),
                FileText = text,

            };
            ParseResumeResponse response = new ParseResumeResponse();
            //getRef.Status = "Calling the service at " + service.Url;
            //Stopwatch sw = Stopwatch.StartNew();
            //off for testing
            try
            {
                response = service.ParseResume(request);
            }
            catch(Exception ex)
            {
                //HtmlDocument hdoc = new HtmlDocument();
                //hdoc.LoadHtml(text);
                //var textString = hdoc.DocumentNode.InnerText;
                string final = Regex.Replace(text, @"<(.|n)*?>", string.Empty).Replace("&nbsp", " ");
                ParseResumeRequest request2 = new ParseResumeRequest
                {
                    AccountId = "42848698",
                    ServiceKey = "o2HTFboiSqprZbKsFBuszZAadbnYXxfoMW21j4EL",
                    Configuration = getRef.GetConfiguration(),
                    FileText = final,

                };

                
                response = service.ParseResume(request2);
            }
            
            int usesleft = response.UsesRemaining;
            ApplicationDbContext db = new ApplicationDbContext();
            var q = db.apis.Find(1);
            q.Worth = usesleft;
            db.SaveChanges();
            //sw.Stop();
            //getRef.Status = "Call complete";

            XDocument doc = XDocument.Parse(response.Xml);
            //XDocument doc = XDocument.Load(HttpContext.Current.Server.MapPath("/Helpers/resumeXML.xml"));

            //string usesleft = response.UsesRemaining.ToString();
            string results = doc.ToString();
           

           

           
            return results;

        }
    }
}