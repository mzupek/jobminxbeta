﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml.XPath;

namespace Bootstrap.Helpers
{
    
    public class Onet
    {
        private string username = "jobminx";
        private string password = "hmx2283";
       
        
        
        public string GetJobCodes (string keyword)
        {

           string url = "http://services.onetcenter.org/ws/mnm/search?keyword="+keyword;

           WebRequest request = WebRequest.Create(url);
           request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(username+":"+password)));
           WebResponse response = request.GetResponse();
           Stream dataStream = response.GetResponseStream();
           // Open the stream using a StreamReader for easy access.
           StreamReader reader = new StreamReader(dataStream);
           // Read the content.
           string xml = reader.ReadToEnd();
           
           // Scrub XML namespaces so XPath is simpler to write
           xml = xml.Replace("xmlns=\"http://ns.hr-xml.org/2006-02-28\"", "");
           xml = xml.Replace("<sov:", "<");
           xml = xml.Replace("</sov:", "</");

           XPathDocument doc = new XPathDocument(new StringReader(xml));
           XPathNavigator nav = doc.CreateNavigator();
           XPathNavigator navDetail;
           navDetail = nav.SelectSingleNode("//code");
           string code = navDetail.Value;
           return code;
        }
        //this call is everything in a nutshell//
        public string GetCareerReport (string code)
        {
            string url = "http://services.onetcenter.org/ws/mnm/careers/" + code;

            WebRequest request = WebRequest.Create(url);
            request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(username + ":" + password)));
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            return responseFromServer;

        }

        public string GetCareerOverview(string code)
        {
            string url = "http://services.onetcenter.org/ws/mnm/careers/"+code+"/report";

            WebRequest request = WebRequest.Create(url);
            request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(username + ":" + password)));
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            return responseFromServer;


        }

        public string GetSkills(string code)
        {
            string url = "http://services.onetcenter.org/ws/mnm/careers/"+code+"/skills";

            WebRequest request = WebRequest.Create(url);
            request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(username + ":" + password)));
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            return responseFromServer;


        }

        public string GetKnowledge(string code)
        {
            string url = "http://services.onetcenter.org/ws/mnm/careers/"+code+"/knowledges";

            WebRequest request = WebRequest.Create(url);
            request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(username + ":" + password)));
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            return responseFromServer;


        }

        public string GetAbilities(string code)
        {
            string url = "http://services.onetcenter.org/ws/mnm/careers/"+code+"/abilities";

            WebRequest request = WebRequest.Create(url);
            request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(username + ":" + password)));
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            return responseFromServer;


        }

        public string GetPersonality(string code)
        {
            string url = "http://services.onetcenter.org/ws/mnm/careers/" + code + "/personality";

            WebRequest request = WebRequest.Create(url);
            request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(username + ":" + password)));
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            return responseFromServer;


        }
        public string GetTechnology(string code)
        {

            string url = "http://services.onetcenter.org/ws/mnm/careers/" + code + "/technology";

            WebRequest request = WebRequest.Create(url);
            request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(username + ":" + password)));
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            return responseFromServer;

        }

        public string GetEducation(string code)
        {

            string url = "http://services.onetcenter.org/ws/mnm/careers/" + code + "/education";

            WebRequest request = WebRequest.Create(url);
            request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(username + ":" + password)));
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            return responseFromServer;

        }

        private string GetValue(XPathNavigator nav, string xpath)
        {
            if (nav == null) return String.Empty;

            nav = nav.SelectSingleNode(xpath);
            if (nav == null)
            {
                return String.Empty;
            }
            return nav.Value;
        }

        private string GetValues(XPathNavigator nav, string xpath)
        {
            if (nav == null) return String.Empty;

            StringBuilder sb = new StringBuilder(100);
            XPathNodeIterator iter = nav.Select(xpath);
            while (iter.MoveNext())
            {
                if (sb.Length > 0)
                {
                    sb.Append(", ");
                }
                sb.Append(iter.Current.Value);
            }
            return sb.ToString();
        }

    }
}