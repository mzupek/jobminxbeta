﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bootstrap.Models;

namespace Bootstrap.Helpers
{
   
    public class FreeTrials
    {
        private JMContext db = new JMContext(); 
        private int Total = 1;
       
        public int TriesLeft(string ip)
        {
            int triesleft = 0;
            var q = db.Attempts.Where(m => m.IpAddress == ip).ToList();
            int userAtt = q.Count();
            int usesleft = Total - userAtt;
            if(usesleft > 0)
            {
               
                var n = db.Attempts.Where(m => m.IpAddress == ip).ToList();
                int userAttn = q.Count();
                int usesleftn = Total - userAttn;
                triesleft = usesleftn;


            }
            else
            {
                triesleft = 0;
            }

            return triesleft;
        }
        
        public int IncrementTries(string ip)
        {

            Attempt instance = new Attempt();
            instance.IpAddress = ip;
            db.Attempts.Add(instance);
            db.SaveChanges();

            var q = db.Attempts.Where(m => m.IpAddress == ip).ToList();
            int userAtt = q.Count();
            int usesleft = Total - userAtt;
            if(usesleft <= 0)
            {
                usesleft = 0;
            }

            return usesleft;
        }
    }
}